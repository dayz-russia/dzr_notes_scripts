modded class MissionServer
{
	ref array<string> m_NullArray = new array<string>;
    
    ref map<string, string> NoteTitles = new map<string, string>();
    
	string m_NullString = "";
    string m_StringConfig;
    int m_IntConfig;
    string m_StringFirstLineFromArray;
    
    int m_NoteUIDs = 0;
	
    string AllowNoteCopy;
    string NotesPerPlayer;
    string SignatureClickURLparam;
    string SignatureClickURL;
    string AdminNoteCharLimit;
    string PlayerNoteCharLimit;
    
    bool cleanedUp = false;
    
	void MissionServer()
	{
		Print("[dzr_notes] ::: Starting Serverside");
		#ifdef AMS_AdditionalMedicSupplies
			Print("[dzr_notes] ::: Detected Additional Medic Supplies. Applying compatibility patch.");
        #endif
		GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionServer_IncrementUIDs", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionServer_SendFileToClient", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionServer_LogNote", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_SyncPreviewOnCLient", this, SingleplayerExecutionType.Both );
		
        DZR_Notes().CheckFolders();
        
		
		ref array<string> m_CfgArray = new array<string>;
		
		//admin
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes/notes_db/static_admin", "NoteNameInText1.html", m_NullArray, "<html><p>Spawn a static admin note (DZR_PaperWrittenAdmin_Static, DZR_PaperWrittenAdmin_Red_Static, DZR_PaperWrittenAdmin_Green_Static, DZR_PaperWrittenAdmin_Blue_Static) and write this file name in it without .html part. When someone tries to read that note, the note will show the text from the file.<br/>So can place 10 notes in ten location and set one text source for all of them. Anytime you change this text in the file, it will be updated in all note in game.<br/><br/>Спавним, например статичную админскую записку (DZR_PaperWrittenAdmin_Static, DZR_PaperWrittenAdmin_Red_Static, DZR_PaperWrittenAdmin_Green_Static, DZR_PaperWrittenAdmin_Blue_Static), но в текст пишем имя файла без .txt и при прочтении в записке будет текст из файла вместо его имени.\r\nТак можно разместить 10 записок в разных локациях и задать всем одинаковый текст. Так же текст можно сразу у всех отредактировать в файле и он сразу заменится в игре.</p><html>");
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes/notes_db/static_admin", "NoteNameInText2.html", m_NullArray, "<html><p>Same here. If any static admin note get whis written in it - NoteNameInText2 -, it will show the this text. You can spawn plain static note, red, green, and blue.<br/><br/>Это второй текст-заготовка. Если вписать в записки NoteNameInText2 вместо текста, то будет показан этот текст из файла.</p></html>");
		
		//type 2
		//m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes/notes_db/personal", "UnlimitedText.html", m_NullArray, "<html>Not implemented yet.<hr>Пока не разработано.</html>");
		
		DeleteFile("$profile:/DZR/dzr_notes/README_EN_v1.txt");
		DeleteFile("$profile:/DZR/dzr_notes/README_EN_v2.txt");
		DeleteFile("$profile:/DZR/dzr_notes/README_EN.txt");
		DeleteFile("$profile:/DZR/dzr_notes/README_EN.html");
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "README_EN.html", m_NullArray, "<h2>How to give a note with your text to the starting gear?</h2><br>Add this code to your init.c into the StartingEquipSetup class after SetRandomHealth( itemEnt );:<br><br>			itemEnt = player.GetInventory().CreateInInventory( \"DZR_PaperWrittenAdmin_Red\" );<br>			DZR_PaperWrittenAdmin_Red red_note = DZR_PaperWrittenAdmin_Red.Cast( itemEnt );<br>			WrittenNoteData m_NoteContents = new WrittenNoteData(red_note);<br>			m_NoteContents.SetNoteText(\"Here goes your text\\r\\nAnd the second line. Use \\r\\n to start new lines of thext.\");<br>			red_note.SetPaperWrittenText(m_NoteContents);<br><br><h2>How to place a permanent read-only note that is persistent forever and only admin can edit it?</h2><br>Use DZR_3MDuctTape on your note, it will get locked forever and noone will be able to take, edit, or remove it. It will be stored persistently even after server restarts. Unlock the note using DZR_3MDuctTape again. It can be used to lock any other notes as well.<br><br>See video for demo:<br>https://www.youtube.com/watch?v=91lok8P8r9A<br><br><h2>How to change text in admin notes offline, without connecting to the server?</h2><br>1. Go to \\DZR\\dzr_notes\\notes_db\\admin in your server profiles folder.<br>2. Create a TXT file. Rename it to your liking without spaces and special characters.<br>3. Edit that file and write the text you want to see in-game on your static note. No empty lines are supported! If you need an empty line, place space character on an empty line. Otherwise, the text will get cut after a new empty line.<br>4. In-game: Use Paper and the DZR_AdminPen_Static item to create a note. Styled admin notes are available too when using other pens: DZR_AdminPen_Red_Static, DZR_AdminPen_Green_Static, DZR_AdminPen_Blue_Static.<br>5. Copypaste the name of your txt file in the note text without (!) .txt extension and press OK to save it.<br>			For example, if you file is \"Test1.txt\", it should be \"Test1\" in the note.<br>6. Now, anytime a player reads the that note, the player will see not the filename, but the text from the TXT file on your server. Any changes in that TXT file on your server will be live immediately in-game.<br><br>See video for demo:<br>https://www.youtube.com/watch?v=a0ejbQB4JMA<br>https://www.youtube.com/watch?v=trN_sW4Uovg<br>");
		
		DeleteFile("$profile:/DZR/dzr_notes/README_RU_v1.txt");
		DeleteFile("$profile:/DZR/dzr_notes/README_RU_v2.txt");
		DeleteFile("$profile:/DZR/dzr_notes/README_RU.txt");
		DeleteFile("$profile:/DZR/dzr_notes/README_RU.html");
		m_StringConfig = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "README_RU.html", m_NullArray, "<h2> Как выдать записку игроку в стартовом снаряжении? </h2><br>Добавьте этот код в ваш init.c в класс StartingEquipSetup после SetRandomHealth( itemEnt );:<br><br>			itemEnt = player.GetInventory().CreateInInventory( \"DZR_PaperWrittenAdmin_Red\" );<br>			DZR_PaperWrittenAdmin_Red red_note = DZR_PaperWrittenAdmin_Red.Cast( itemEnt );<br>			WrittenNoteData m_NoteContents = new WrittenNoteData(red_note);<br>			m_NoteContents.SetNoteText(\"Тут ваш текст.\\r\\nА это вторая строка.\\r\\nНовые строки разделяются \\r\\n\");<br>			red_note.SetPaperWrittenText(m_NoteContents);<br><br><h2>Как разместить записку, которую игроки не смогут взять или изменить, и которая сохраняется после рестарта? </h2><br>Используйте DZR_3MDuctTape на вашей записке, чтобы заблокировать и тогда никто не сможет её взять или изменить. Её можно будет только читать и она сохранится даже после рестарта. Разблокировать записку можно повторным применением DZR_3MDuctTape на записке. <br><br>Подробнее на видео:<br>https://www.youtube.com/watch?v=91lok8P8r9A<br><br><h2>Как изменить текст в админских записках без входа в игру?</h2><br>1. Откройте папку \\DZR\\dzr_notes\\notes_db\\admin в папке профиля на вашем сервере.<br>2. Создайте TXT файл и переименуйте как вам хочется, но без пробелов и спецсимволов.<br>3. Откройте этот файл редактором и впишите текст, который хотите увидеть в записке. Пустые строки не поддерживаются! Если нужна пустая строка, поставьте там пробел, иначе после пустой строки остальной текст будет удалён.<br>4. В игре: Используйте Paper и DZR_AdminPen_Static для создания записки. Стилизованные создаются также другими ручками: DZR_AdminPen_Red_Static, DZR_AdminPen_Green_Static, DZR_AdminPen_Blue_Static.<br>5. Скопируйте в вашу записку имя вашего TXT файла без (!) расширения .txt и нажмите ОК, чтобы сохранить текст.<br>		Например, если ваш файл называется \"Test1.txt\", то в записке должно быть только \"Test1\".<br>6. Теперь каждый раз, когда игрок будет читать эту записку, он увидит не имя файла, текст из этого файл с сервера. Все изменения в файле на сервере сразу появятся в записке.<br><br>https://www.youtube.com/watch?v=a0ejbQB4JMA<br>https://www.youtube.com/watch?v=trN_sW4Uovg<br>");
		
		PlayerNoteCharLimit = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "PlayerNoteCharLimit.txt", m_NullArray, "800\r\n\r\nHow many characters can players enter when writing or editing non-admin notes. Max 2800 maximum in the engine.\r\n\r\nСколько символов игроки могут вводить при написании или редактировании неадминской записки. 2800 максимально в движке.");
		
		AdminNoteCharLimit = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "AdminNoteCharLimit.txt", m_NullArray, "2800\r\n\r\nHow many characters can be entered when writing or editing admin notes. Max 2800 maximum in the engine.\r\n\r\nСколько символов можно ввести при написании или редактировании админской записки. 2800 максимально в движке.");
		
		SignatureClickURL = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "SignatureClickURL.txt", m_NullArray, "https://steamcommunity.com/profiles/\r\n\r\nBase URL for opening after clicking on the signature name. Can be appended by SignatureClickURLparam. Set to 0 to disable.\r\n\r\nБазовый URL, который откроется после клика на имени подписи. Можно добавить к URL параметр из CustomURLparam. 0 - отключить открытие страницы.");
        
		SignatureClickURLparam = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "SignatureClickURLparam.txt", m_NullArray, "steam_id\r\n\r\nSelect the parameter to append to CustomURL. Possible values: player_name, steam_id, 0. For example with those default settings clicking on a signature name will append player's Steam ID to this url: https://steamcommunity.com/profiles/. So if DZR Mikhail signs a note, clicking the signature will open this page in the browser: https://steamcommunity.com/profiles/76561198000801794\r\n\r\nРежим добавления параметра к CustomURL. Возможные значения: player_name, steam_id, 0. Например, с дефолтными настройками при нажатии на имя подписи откроется страница https://steamcommunity.com/profiles/ и к ней подставится Steam ID игрока, таким образом, если страницу подпишет DZR Mikhail, то в браузере откроется страница https://steamcommunity.com/profiles/76561198000801794");
		
        NotesPerPlayer = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "NotesPerPlayer.txt", m_NullArray, "Not implemented yet.\r\n\r\nПока не работает.");
		
        AllowNoteCopy = dzr_notes_cfg_CFG.GetFile("$profile:/DZR/dzr_notes", "AllowNoteCopy.txt", m_NullArray, "Not implemented yet.\r\n\r\nПока не работает.");
        
		//m_IntConfig = dzr_notes_cfg_CFG.GetFile("$profile:\\DZR\\dzr_notes", "IntConfig.txt", m_NullArray, "1").ToInt();
		//m_StringFirstLineFromArray = dzr_notes_cfg_CFG.GetFile("$profile:\\DZR\\dzr_notes", "ArrayConfig.txt", m_CfgArray, "Line1\r\nLine2");
		
        
        
		
    }
    
    
    
    void CleanupNoteFiles2()
    {
        Print("\r\n\ trying to cleanup");
        if (GetGame().IsServer())
        {
            map<string,vector> ItemPositons = new map<string,vector>;
            //array<vector> ScanPockets = GetScanPositions();
            
            array<EntityAI> entities = new array<EntityAI>;
            
            string typeTosearch = "DZR_PaperWritten";
            typename t = typeTosearch.ToType();
            if (t && (t.IsInherited(CrashBase) || t.IsInherited(House) || t.IsInherited(BuildingSuper)))
            {
                //Land_ / House hybrid type search (SLOW!)
                DayZPlayerUtils.PhysicsGetEntitiesInBox("0.0 -1200.0 0.0", "20000.0 1200.0 20000.0", entities);
                }else{
                DayZPlayerUtils.SceneGetEntitiesInBox("0.0 -1200.0 0.0", "20000.0 1200.0 20000.0", entities);
            }
            Print("entities.Count()");
            Print(entities.Count());
            ref array<string> allPids = new array<string>();
            for (int i = 0; i < entities.Count(); ++i)
            {
                EntityAI ent = entities[i];
                //Print(ent.GetType());
                if (ent && ent.IsKindOf(typeTosearch))
                {
                    int b1, b2, b3, b4;
                    string notePid;
                    EntityAI target_object = ent;
                    if(target_object)
                    {
                        target_object.GetPersistentID(b1, b2, b3, b4);
                    }
                    
                    string m_NotePID = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
                    
                    Print("\r\n\r\n========== Found DZR_PaperWritten on map, PID: "+m_NotePID);
                    allPids.Insert(m_NotePID+".html");
                    //DZR_Notes().RegisterNewNote(m_NotePID, target_object);
                    //RegisterNetSyncVariableBool("m_DZR_ItemNote");
                    // m_DZR_IsLocked = false;
                    // m_DZR_ItemNote = "";
                    
                    
                }
            }
            
            string	file_name;
            int 	file_attr;
            int		flags;
            
            string m_DZRdir = "\\dzr_notes\\notes_db\\personal";
            string extension = "html";
            string rootDir = "$profile:\\DZR";
            //string path_find_pattern = m_DZRdir+"\\*"; //*/
            string fullPath = rootDir+"\\"+m_DZRdir+"\\";
            Print("Trying to find all "+extension+" files in "+fullPath);
            FindFileHandle file_handler = FindFile(fullPath+"\\*."+extension, file_name, file_attr, flags);
            
            
            
            bool found = true;
            int counter = 0;
            bool filesExist = false;
            
            FileHandle fhandle;
            while ( found )
            {				
                
                Print("Found "+extension+" file: "+file_name);
                if(allPids.Find(file_name) == -1)
                {
                    Print("Not found among loaded notes: "+file_name);
                    string source = "$profile:DZR/dzr_notes/notes_db/personal/"+file_name;
                    string dest = "$profile:DZR/dzr_notes/notes_db/archive/"+file_name;
                    if(CopyFile(source,dest))
                    {
                        Print("Coped to archive: "+source);
                        if(DeleteFile(source))
                        {
                            Print("Deleted: "+source);
                        }
                    }
                }
                
                found = FindNextFile(file_handler, file_name, file_attr);
            }
        }
    }
    
    void CleanupNoteFiles()
    {
        if(!GetGame().IsMultiplayer() || GetGame().IsServer())
        {
            Print("\r\n\r\n------------ CleanupNoteFiles -------------\r\n");
            ref array<string> allPids;
            allPids = DZR_Notes.GetAllPids();
            Print(allPids);
            Print(allPids.Count());
            
            string m_DZRdir = "\\dzr_notes\\notes_db\\personal";
            string extension = "html";
            
            string	file_name;
            int 	file_attr;
            int		flags;
            TStringArray list = new TStringArray;
            //"$profile:\\DZR\\"
            string rootDir = "$profile:\\DZR";
            //string path_find_pattern = m_DZRdir+"\\*"; //*/
            string fullPath = rootDir+"\\"+m_DZRdir+"\\";
            Print("Trying to find all "+extension+" files in "+fullPath);
            FindFileHandle file_handler = FindFile(fullPath+"\\*."+extension, file_name, file_attr, flags);
            
            
            bool found = true;
            int counter = 0;
            bool filesExist = false;
            
            FileHandle fhandle;
            while ( found )
            {				
                
                Print("Found "+extension+" file: "+file_name);
                if(allPids.Find(file_name) == -1)
                {
                    Print("Not found among loaded notes: "+file_name);
                    string source = "$profile:DZR/dzr_notes/notes_db/personal/"+file_name+".html";
                    string dest = "$profile:DZR/dzr_notes/notes_db/archive/"+file_name+".html";
                    if(CopyFile(source,dest))
                    {
                        Print("Coped to archive: "+source);
                        if(DeleteFile(source))
                        {
                            Print("Deleted: "+source);
                        }
                    }
                }
                
                found = FindNextFile(file_handler, file_name, file_attr);
            }
            
        }
        
    };
    
    
    
    override void InvokeOnConnect(PlayerBase player, PlayerIdentity identity)
    {
        super.InvokeOnConnect(player, identity);		
        //player.SavePlayerName(identity.GetName());
        ////Print("[DZR IdentityZ] ::: MissionServer InvokeOnConnect ::: Player name stored to CharacterName: "+identity.GetName());
        //auto newIdentityZ = new Param2<PlayerBase, string>(player, identity.GetName());
        //GetGame().RPCSingleParam(player, DZRIDZRPC.SAVEPLAYERNAME, newIdentityZ, true);
        //Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect");
        
        
        ref Param6<string, string, string, string, string, string> m_Data = new Param6<string, string, string, string, string, string>(AllowNoteCopy, NotesPerPlayer, SignatureClickURLparam, SignatureClickURL, AdminNoteCharLimit, PlayerNoteCharLimit);
        //Print("[DZR IdentityZ] ::: MissionServer ::: InvokeOnConnect ReadServerConfig "+m_Data);
        GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_ReceiveClientsideSettings", m_Data, true, identity);
    }
    /*
        void DZR_MissionServer_LogNote(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
        {
        Print("[dzr_notes] ::: Got RPC from client");
        ref Param1< string > data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
        if (data.param1 != "")
        {
        
        string steam_id = sender.GetPlainId();
        string player_name = sender.GetName();
        DZR_MissionServer_CheckFolders();
        PlayerBase thePlayer = dzr_notes_cfg_CFG.dzrGetPlayerByIdentity(sender);
        vector plr_pos = thePlayer.GetPosition();
        SaveServerSide(player_name, steam_id, data.param1, plr_pos[0].ToString()+" "+plr_pos[1].ToString()+" "+plr_pos[2].ToString());
        //Print("DZR_MissionServer_LogNote(data.param1, sender);:" +data.param1);
        
        }
        }
        }
    */
    
    void DZR_MissionServer_IncrementUIDs()
    {
        m_NoteUIDs++;
    }
    
	override void OnUpdate(float timeslice)
    {
        super.OnUpdate(timeslice);
        if(GetHive().IsIdleMode() && !cleanedUp)
        {
            cleanedUp = true;
            CleanupNoteFiles2();
        }
    }
    
    void DZR_SyncPreviewOnCLient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
    {
        // Print("[dzr_notes] ::: Got RPC from client");
        ref Param1< DZR_PaperWritten > data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
            if(data.param1)
            {
                Print("\r\n\============DZR_SyncPreviewOnCLient");
                
                ///PID
                int b1, b2, b3, b4;
                string notePid_mg;
                EntityAI target_object = data.param1 ;
                if(target_object)
                {
                    target_object.GetPersistentID(b1, b2, b3, b4);
                }
                
                notePid_mg = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
                Print(notePid_mg);
                ///PID
                
                ref array<string> signers = new array<string>();
                ref array<string> signers_dates = new array<string>();
                string filePath = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid_mg+".html";
                
                
                
                //TODO add text formating here. Just text string for now
                string fullText;
                if ( FileExist( filePath ) )
                {
                    fullText = DZR_Notes().LoadNoteMeta(filePath, signers, signers_dates);
                };
                
                ref array<string> textSplit = new array<string>();
                
                fullText.Split("\n", textSplit);
                fullText = textSplit.Get(0);
                int cutTo = 45;
                if(fullText.LengthUtf8() >= cutTo)
                {
                    fullText = fullText.SubstringUtf8(0, cutTo);
                }
                ;Print(fullText)                
                ref Param2<string, string> m_Data = new Param2<string, string>(fullText, notePid_mg);
                GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_UpdateCurrentTitleOnCLient", m_Data, true, sender);
            }
        }
    }
    
    void DZR_MissionServer_SendFileToClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
    {
        Print("[dzr_notes] ::: Got RPC from client");
        ref Param1< string > data;
        if ( !ctx.Read( data ) ) return;
        if (type == CallType.Server)
        {
            if (data.param1 != "")
            {
                
                //bool result =  data.param1.ToggleShowNameToAll();
                Print("SendFileToClient(data.param1, sender);:" +data.param1);
                SendFileToClient(data.param1, sender);
                
            }
        }
    }
    
    void SendFileToClient(string m_TxtFileName, PlayerIdentity identity)
    {
        ref array<string> compressed = new array<string>;
        JsonSaveData packed_text_object = new JsonSaveData();
        
        // READ FILE CONTENTS
        string content;
        FileHandle fhandle;
        //NoteTitles
        if (FileExist(m_TxtFileName))
        {
            //Print("DZR_PaperWritten_-14782089181514317906688471483907209516.json EXISTS!");
            fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
            string line_content;
            while ( FGets( fhandle,  line_content ) > 0 )
            {
                content += line_content;
            }
            CloseFile(fhandle);
        }
        else
        {
            //Print("NO FILE! "+ m_ObjectFileName);
        }
        // READ FILE CONTENTS
        
        compressed = DZR_Notes().PackText(content);
        packed_text_object.text = compressed;
        
        
        
        //save it
        string m_SaveObjectFileName = "$profile:DZR/dzr_notes/pre_saved_text.json";
        //JsonFileLoader< JsonSaveData >.JsonSaveFile(m_SaveObjectFileName, importedConfig.text);
        JsonFileLoader< array<string> >.JsonSaveFile(m_SaveObjectFileName, compressed);
        //save it
        
        ref Param1<JsonSaveData> m_Data = new Param1<JsonSaveData>(packed_text_object);
        GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", m_Data, true, identity);	
        //Print("[dzr_notes] ::: Sent RPC to client");
    }
    
    
    void SaveServerSide(string PlayerName, string steam_id, string Notetext, string ps, string notePid, ref array<string> m_ArrayFromFile)
    {
        int hour;
        int minute;
        int second;
        int year;
        int month;
        int day;
        GetHourMinuteSecond(hour, minute, second);
        GetYearMonthDay(year, month, day);
        string suffix = year.ToString()+"-"+month.ToString()+"-"+day.ToString()+"_"+hour.ToString()+"."+minute.ToString()+"."+second.ToString();
        string m_ObjectFileName = "$profile:DZR/dzr_notes/log/"+steam_id+"_"+PlayerName+"_"+suffix+".txt";
        string m_PidFileName = "$profile:DZR/dzr_notes/notes_db/personal/"+notePid+".html";
        FileHandle fhandle;
        if ( !FileExist(m_ObjectFileName) )
        {
            
            FileSerializer file = new FileSerializer;
            //FileHandle file = OpenFile(m_ObjectFileName, FileMode.APPEND);
            if (file.Open( m_ObjectFileName, FileMode.WRITE) )
            {
                file.Write(Notetext); //ps
                //FPrintln(file, Notetext);
                //FPrintln(file, ps);
                file.Close();
                //Print("missionServer.c ::: "+PlayerName + " ("+steam_id+") wrote text: " +Notetext+ " (logged to file: "+m_ObjectFileName+")");
            }
            else
            {
                //Print("Failed write "+m_ObjectFileName);
            }
            
            FileHandle file2 = OpenFile(m_PidFileName, FileMode.WRITE);
            if (file2)
            {
                string text;
                if ( file2 )
                {
                    //Print("missionServer.c SaveServerSide : println to file " + m_PidFileName);
                    //Print(" missionServer.c SaveServerSide m_ArrayFromFile.Count() " + m_ArrayFromFile.Count());
                    //FPrintln(file, "<body>");
                    for (int i = 0; i < m_ArrayFromFile.Count(); i++; )
                    {
                        text +=  m_ArrayFromFile.Get(i);
                        FPrint(file2, m_ArrayFromFile.Get(i));
                        
                    }
                    //FPrintln(file, "</body>");
                    //Print("FPrintln(file, text):" + text);
                    CloseFile(file2);
                    Print("missionServer.c ::: "+PlayerName + " ("+steam_id+") created note: "+m_PidFileName);
                }
                else
                {
                    Print("missionServer.c ::: Could not create note: "+m_PidFileName);
                }
                
            }
            else
            {
                Print("Failed write "+m_ObjectFileName);
            }
            
            
        } 
    }
    
    
    
}    