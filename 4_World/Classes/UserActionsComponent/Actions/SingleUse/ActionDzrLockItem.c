modded class PlayerBase extends ManBase
{
	override void SetActions(out TInputActionMap InputActionMap)
	{
		AddAction(ActionDzrLockItem, InputActionMap);
		super.SetActions(InputActionMap);
    }
}

class ActionDzrLockItem: ActionSingleUseBase
{
	void ActionDzrLockItem()
	{
		//m_CommandUID        = DayZPlayerConstants.CMD_ACTIONMOD_OPENDOORFW;
		m_CommandUID        = DayZPlayerConstants.CMD_ACTIONMOD_ATTACHITEM;
		m_StanceMask        = DayZPlayerConstants.STANCEMASK_CROUCH | DayZPlayerConstants.STANCEMASK_ERECT;
		
    }
	override bool IsInstant()
	{
		return false;
    }
	override void CreateConditionComponents()  
	{	
		m_ConditionTarget = new CCTNonRuined( UAMaxDistances.DEFAULT );
		m_ConditionItem = new CCINonRuined;
    }
	
	/*
		override typename GetInputType()
		{
		return ToggleLightsActionInput;
		}
		
		override bool HasTarget()
		{
		return false;
		}
    */
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{	
		ItemBase tgt_item = ItemBase.Cast( target.GetObject() );
		//ItemBase hands_item = ItemBase.Cast( item.GetObject() );
		if ( tgt_item && item.IsKindOf( "DZR_3MDuctTape" ) ) 
		{
			if ( tgt_item.IsTakeable() )
			{
				m_Text 	= "#STR_DZR_LockItem";
				//Print(tgt_item.IsTakeable());
            }
			else
			{
				m_Text 	= "#STR_DZR_UnlockItem";
				//Print(tgt_item.IsTakeable());
            }
			return true;
        }
		return false;
    }
	
    string GetTimestamp()
    {
		string theTimestamp;
		
		int hour;
		int minute;
		int second;
		int year;
		int month;
		int day;
		
		GetHourMinuteSecond(hour, minute, second);
		GetYearMonthDay(year, month, day);
        //Print( (year + month + day).ToString() +  (hour/60 +  minute/60 +  second).ToString() );
		//theTimestamp = ( (year + month + day).ToString() +  (day +  minute +  second).ToString() ).ToInt() ;
		theTimestamp = (year + month + day).ToString() +  (day +  minute +  second).ToString() ;
		return theTimestamp;
    }
	
	override void Start( ActionData action_data ) //Setup on start of action
	{
		super.Start( action_data );
		DZR_PaperWritten targetItem = DZR_PaperWritten.Cast(action_data.m_Target.GetObject() );
		//Print(targetItem);
		//Print("!targetItem.IsTakeable(): "+!targetItem.IsTakeable() );
		if(!targetItem)
		{
			return;
        }
		
        
		targetItem.DZR_SetLocked( targetItem.IsTakeable() );
		targetItem.DZR_SetItemNote( "Itemnote: "+targetItem + ": " + GetTimestamp() );
		targetItem.SetTakeable( !targetItem.IsTakeable() );
		targetItem.SetLifetime(3888000);
        targetItem.SetAllowDamage(targetItem.IsTakeable());
        
		targetItem.SetSynchDirty();
        
        if(targetItem.IsTakeable())
        {
			Print("\r\n[dzr_notes] ::: Item Lock");
			Print(action_data.m_Player);
			Print("     Item "+targetItem+" is locked at "+targetItem.GetPosition() );
            Print("     targetItem.GetAllowDamage()::::");
            Print(targetItem.GetAllowDamage());
        } 
		else
		{
			Print("\r\n[dzr_notes] ::: Item UnLock");
			Print(action_data.m_Player);
			Print("     Item "+targetItem+" is unlocked at "+targetItem.GetPosition() );
            Print("     targetItem.GetAllowDamage()::::");
            Print(targetItem.GetAllowDamage());
			//GetDayZGame().RequestExit(IDC_MAIN_QUIT);
        }
        
		action_data.m_Player.StartDZRDuctSound();
		GetGame().GetCallQueue( CALL_CATEGORY_GAMEPLAY ).CallLater( action_data.m_Player.ResetDZRDuctSound, 2000 );
    }
	
	
};	