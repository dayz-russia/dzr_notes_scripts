class ActionReadNoteOnGround: ActionInteractBase
{
    PlayerBase dzr_m_Player;
	/*
		#ifdef AMS_AdditionalMedicSupplies
		protected ref AMS_TestKitReportMenu m_Report1;
		#endif
    */
	void ActionReadNoteOnGround()
	{
		m_CommandUID = DayZPlayerConstants.CMD_ACTIONMOD_INTERACTONCE;
		m_StanceMask = DayZPlayerConstants.STANCEMASK_ERECT | DayZPlayerConstants.STANCEMASK_CROUCH;
        m_Text = "#read";
    }
	

    
	override bool ActionCondition ( PlayerBase player, ActionTarget target, ItemBase item )
	{
		Paper target_object = Paper.Cast(target.GetObject());
		dzr_m_Player = player;
        
        
        
		if (player.IsPlacingLocal())
		return false;
		
		if ( player && target_object && target_object.IsReadable() )
		{

			return true;
        }
		
		return false;
    }
	
    override void OnStart( ActionData action_data )
    {
        super.OnStart( action_data );
		/*
			#ifdef AMS_AdditionalMedicSupplies
			m_Report1 = null;
			AMS_TestKitReport report = AMS_TestKitReport.Cast( action_data.m_Target.GetObject() );
			Print("OnStart AMS");
			if ( report && !m_Report1 )
			{
			if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
			{
			Print("AMS success, open Report");
			m_Report1 = new AMS_TestKitReportMenu( report );
			GetGame().GetUIManager().ShowScriptedMenu( m_Report1, NULL );
			}
			
			}
			#endif
        */
		if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
		{	
			if (GetGame().GetUIManager() && GetGame().GetUIManager().IsMenuOpen(MENU_NOTE)) 
			{
				GetGame().GetUIManager().FindMenu(MENU_NOTE).Close();
            }
        }
    }
	
	override void OnStartServer( ActionData action_data )
	{
        ActionData m_ActionData = action_data;
		ItemBase paper_item = ItemBase.Cast(action_data.m_Target.GetObject());
		bool isAMS = false;
		/*
			#ifdef AMS_AdditionalMedicSupplies
			Print(":::AMS detected:::");
			AMS_TestKitReport report = AMS_TestKitReport.Cast( action_data.m_Target.GetObject() );
			if(report)
			{
			isAMS = true;
			Print("AMS target is report" + report + " - " + m_Report1);
			}
			#endif
        */
		if(paper_item && !isAMS && (!GetGame().IsMultiplayer() || GetGame().IsServer()) )
		{
		
			//getting PID
			PlayerBase player = PlayerBase.Cast( m_ActionData.m_Player );
			//ItemBase paper_item = ItemBase.Cast( player.GetItemInHands() );
			int b1, b2, b3, b4;
			string notePid;
			EntityAI target_object = paper_item ;
			if(target_object)
			{
				target_object.GetPersistentID(b1, b2, b3, b4);
            }
			
			notePid = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
			//del
			
			if(notePid == "0000")
			{
				Print("NO PID");
            };
			//getting PID
			
			JsonSaveData FileData = new JsonSaveData();
			FileData = SendFileToClient("$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid+".html");
			
			Param2<ref JsonSaveData, string> text = new Param2<ref JsonSaveData, string>(FileData, notePid);
			//Print("actionreadpaper.c notePid:" + notePid);
			//Print("actionreadpaper.c OnStateChange FileData.text[0]:" + FileData.text[0]);
			//Print("actionreadpaper.c OnStateChange FileData.text:" + FileData.text);
			//Print("actionreadpaper.c OnStateChange FileData:" + FileData);
			//Param1<JsonSaveData> text = new Param1<JsonSaveData>(paper_item.GetWrittenNoteData().GetNoteText());
			paper_item.RPCSingleParam(ERPCs.RPC_READ_NOTE, text, true,m_ActionData.m_Player.GetIdentity());
			
        }
		/*
			#ifdef AMS_AdditionalMedicSupplies	
			Print("Checking report");
			if ( report && !m_Report1 )
			{
			Print("Reports true");
			if ( GetGame().IsClient() || !GetGame().IsMultiplayer() )
			{
			Print("AMS success, open Report");
			m_Report1 = new AMS_TestKitReportMenu( report );
			GetGame().GetUIManager().ShowScriptedMenu( m_Report1, NULL );
			
			}
			}
			else
			{
			Print("report && !m_Report1 are false");
			}
			
			#endif
        */		
    }
	
	JsonSaveData SendFileToClient(string m_TxtFileName)
	{
		ref array<string> compressed = new array<string>;
		JsonSaveData packed_text_object = new JsonSaveData();
		
		// READ FILE CONTENTS
		string content;
		FileHandle fhandle;
		if (FileExist(m_TxtFileName))
		{
			Print("ActionReadNoteOnGround.c JsonSaveData SendFileToClient m_TxtFileName");
			Print(m_TxtFileName);
			fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				content += line_content; 
            }
			CloseFile(fhandle);
        }
		else
		{
			Print("ActionReadNoteOnGround.c NO FILE! "+ m_TxtFileName);
        }
		// READ FILE CONTENTS
		compressed = DZR_Notes().PackText(content);
		packed_text_object.text = compressed;
		
		
		
		//save it
		//string m_SaveObjectFileName = "$profile:DZR/dzr_notes/pre_saved_text.json";
		//JsonFileLoader< JsonSaveData >.JsonSaveFile(m_SaveObjectFileName, importedConfig.text);
		//JsonFileLoader< array<string> >.JsonSaveFile(m_SaveObjectFileName, compressed);
		//save it
		
		return packed_text_object;
		//ref Param1<JsonSaveData> m_Data = new Param1<JsonSaveData>(packed_text_object);
		//GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", m_Data, true, identity);	
		//Print("[dzr_notes] ::: Sent RPC to client");
    }

    
}			