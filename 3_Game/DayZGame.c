/**
	Билдить скрипты только Addon Builder
	P:\dzr_notes_unlimited_proto
	P:\@dzr_notes\Addons
	czzzz_DzrNotesScripts
	
	TODO:
    Notes cleaned if in cargo or inventory, or hands. Make a list of notes OnLocationChanged\MovedToCargo?
    Test admin notes
    ? Text is cut in note view WTF
    
**/


class JsonSaveData
{
	string CreatedBy;
	string NoteTitle;
	string CreatedBySteamId;
	string CreatedDate;
	string ModifiedBy;
	string ModifiedBySteamid;
	string ModifiedByDate;
	ref array<string> text;
    /**
        show_created_by
        show_creator_steam_id
        show_modifier
        show_modifier_steam_id
        show_viewers
        show_viewers_steam_id
        steam_ids_clickable
        allow_copy
        show_copy_source_author
        show_copies_made_number
        allow_editable_copy
        allow_comments
        allow_voting
        clan_only
        friends_only
        completed_readonly
    **/
	ref array<string> attributes; 
	ref array<string> signers;
	ref array<string> signers_dates;
	ref array<string> consumed_objects;
	ref array<string> viewers;
	ref array<string> viewers_dates;
	ref array<string> whitelist_steamid;
	
	void JsonSaveData()
	{
		text = new array<string >;
		attributes = new array<string >;
		signers = new array<string >;
		signers_dates = new array<string >;
		consumed_objects = new array<string >;
		viewers = new array<string >;
		viewers_dates = new array<string >;
		whitelist_steamid = new array<string >;
    }
}

class DZR_Notes
{
    static ref array<string> AllNotesPids = new ref array<string>() ;
    
    static void RegisterNewNote(string pid,  EntityAI target_object)
    {

            Print("\r\n\r\n-------------Received a pid: "+pid);
            
            int b1, b2, b3, b4;
            string notePid;
            
            if(target_object)
            {
                target_object.GetPersistentID(b1, b2, b3, b4);
            }
            
            string m_NotePID = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
            
            Print("\r\n\r\n========== Note pid: "+pid);
            Print("========== Note Server Pid: "+m_NotePID);
            
            AllNotesPids.Insert(pid);
            Print("New AllNotesPids count");
            Print(AllNotesPids.Count());
        
    }
    
    static ref array<string> GetAllPids()
    {
        return AllNotesPids;
    }
    
    void CheckFolders()
    {
		string dzr_notes_ProfileFolder = "$profile:\\";
		string dzr_notes_TagFolder = "DZR\\";
		string dzr_notes_ModFolder = "dzr_notes\\";
		string dzr_notes_SubFolder = "log\\";
		string dzr_notes_SubFolder3 = "temp\\";
		string dzr_notes_SubFolder2 = "notes_db\\";
		string dzr_notes_SubFolder4 = "static_admin\\";
		string dzr_notes_SubFolder5 = "personal\\";
		string dzr_notes_SubFolder6 = "archive\\";
        
        string full_notes = dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder2;
		
		//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile");
		if (!FileExist(dzr_notes_ProfileFolder+dzr_notes_TagFolder)){
			//NO DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO DZR");
			MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder);	
			MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder);	
			//dzr_writeConfigFile();			
        }
		else 
		{
			//YES DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile YES DZR");
			if (!FileExist(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder)){
				//NO IDZ
				//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO IDZ");
				
				MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder);
				
				
				//dzr_writeConfigFile();
            }
			else 
			{
				if (!FileExist(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder)){
					MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder);
                }
                //DZR_DeleteFolder(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder2);
                MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder2);
                MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder2+dzr_notes_SubFolder6);
                
                if (!FileExist(full_notes+dzr_notes_SubFolder5))
                {
                    MakeDirectory(full_notes+dzr_notes_SubFolder5);
                };
                
                
                DZR_DeleteFolder(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder3);
                
                MakeDirectory(dzr_notes_ProfileFolder+dzr_notes_TagFolder+dzr_notes_ModFolder+dzr_notes_SubFolder3);
                if (!FileExist(full_notes+dzr_notes_SubFolder4))
                {
                    MakeDirectory(full_notes+dzr_notes_SubFolder4);
                };
                
                
                
                
            }
            
        };
        
        
        
        //PROPER CONFIG
        
    }
    
    int DZR_DeleteFolder(string m_DZRdir)
    {
        
        if(m_DZRdir == "$profile:DZR/") {return false;};
        if(m_DZRdir == "$profile:DZR") {return false;};
        
        string	file_name;
        int 	file_attr;
        int		flags;
        
        string fullPath = m_DZRdir;
        
        FindFileHandle file_handler = FindFile(fullPath+"/*", file_name, file_attr, flags);
        
        bool found = true;
        int file_counter = 0;
        int folder_counter = 0;
        bool noErrors = true;
        
        FileHandle fhandle;
        while ( found )
        {	
            if(file_attr != FileAttr.DIRECTORY)
            {
                noErrors = noErrors + DeleteFile( fullPath+"/"+file_name ); 
                file_counter++;
            } 
            else
            {
                folder_counter = folder_counter + DZR_DeleteFolder(fullPath+file_name+"/");
                DeleteFile( fullPath+"/"+file_name ); 
                folder_counter++;
            };
            found = FindNextFile(file_handler, file_name, file_attr);
        }
        DeleteFile( fullPath ); 
        Print ("[dzr_framework] ::: DELETING "+fullPath+" ------- Deleted files: "+file_counter+". Deleted folders: "+folder_counter );
        return file_counter + folder_counter;
    }
    
    int GetStorageVersion()
    {
        if ( !GetGame().IsServer() )
        {
            return 0;
        }
        string filePath = "$profile:DZR\\dzr_notes\\storage_version.txt";
        string fileContent ="";
        if ( FileExist( filePath ) )
        {
            FileHandle fhandle	=	OpenFile(filePath, FileMode.READ);		
            FGets( fhandle,  fileContent );
            CloseFile(fhandle);
        }
        else
        {
            fhandle	=	OpenFile(filePath, FileMode.WRITE);		
            FPrint( fhandle,  0 );
            CloseFile(fhandle);
        }
        return fileContent.ToInt();
    }
    
    void SetStorageVersion(int new_version)
    {
        if ( !GetGame().IsServer() )
        {
            return;
        };
        
        string filePath = "$profile:DZR\\dzr_notes\\storage_version.txt";
        
        FileHandle fhandle	=	OpenFile(filePath, FileMode.WRITE);		
        if ( FileExist( filePath ) )
        {
            FPrint( fhandle,  new_version );
            CloseFile(fhandle);
        }
        else
        {
            FPrint( fhandle,  0 );
            CloseFile(fhandle);
        }
    }
    
    ref array<string, string> DecodeHtml(string html_text)
    {
        html_text.Replace("\r","");
        html_text.Replace("\n","");
        html_text.Replace("<!DOCTYPE html>","");
        html_text.Replace("<html>","");
        html_text.Replace("<head>","");
        html_text.Replace("</head>","");
        html_text.Replace("<meta http-equiv='content-type' content='text/html; charset=UTF-8'/>","");
        //text3.Replace("<p>","");
        html_text.Replace("</p>","");
        html_text.Replace("</body>","");
        html_text.Replace("</html>","");
        
        Print("\r\n\r\n========DecodeHtml========");
        Print(html_text);
        
        //deHTML
        ref array<string> strFileParam = new array<string >;
        strFileParam = Explode( "<p>", html_text ); //text
        Print(strFileParam);
        string line_content = strFileParam.Get(1);
        Print(line_content);
        string brConverted = line_content;
        
        brConverted.LengthUtf8();
        
        brConverted.Replace("<br/>","\n");
        Print("\r\n------------------- brConverted.LengthUtf8()");
        int brUtf = brConverted.LengthUtf8() - 289;
        Print(brConverted.LengthUtf8());
        Print(brUtf);
        if(brUtf >= 1690)
        {
            brConverted = "text_edit_limit";
        }
        ref array<string> strFileParam2 = new array<string >;
        strFileParam2 = Explode( "<dzr_meta>", strFileParam.Get(0) );
        string meta = strFileParam2.Get(1);
        meta.Replace("</dzr_meta>","");
        
        
        ref array<string> html_decode_result = new array<string>();
        html_decode_result.Insert(brConverted);
        html_decode_result.Insert(meta);
        Print(html_decode_result);
        return html_decode_result;
    }
    
    ref array<string> Explode(string delimiter, string s, bool trim = false) {
        ref array<string> tokens = new ref array<string>();
        int pos = 0;
        string token;
        //DPrint("\r\n\ Exploding string");
        while ( s.IndexOf(delimiter) != -1) 
        {
            pos = s.IndexOf(delimiter);
            
            
            //DPrint("Found delimiter "+delimiter+" at "+pos);
            token = s.Substring(0, pos + delimiter.Length() );
            //DPrint("Got the text from 0 to "+pos+": "+token);
            s.Replace(token, "");
            //DPrint("Deleted token text from string. New String: "+s);
            token.Replace(delimiter, "");
            //DPrint("Clened up the delimiter from token text");
            if(trim)
            {
                token = token.Trim();
            }
            if(trim)
            {
                if(token != "")
                {
                    tokens.Insert(token);
                }
            }
            else
            {
                tokens.Insert(token);
            }
            //pos + delimiter.length());
        }
        if(trim)
        {
            if(s != "")
            {
                tokens.Insert(s);
            }
        }
        else
        {
            tokens.Insert(s);
        }
        //DPrint("String exploded");
        
        //DPrint(tokens);
        return tokens;
    }
    
    string LoadNoteMeta(string filePath, out ref array<string> signers, out ref array<string> signers_dates)
    {
        Print("\r\n\r\n------------------------ UNPACKING META ------------------");
        ref array<string> decoded_html
        if(FileExist(filePath))
        {
            
            string fileContent;
            string line_content;
            FileHandle fhandle	=	OpenFile(filePath, FileMode.READ);		
            while ( FGets( fhandle,  line_content ) > 0 )
            {
                Print("LINE: "+line_content);
                fileContent += line_content.Trim();
                
            }
            CloseFile(fhandle);
            
            decoded_html = DZR_Notes().DecodeHtml(fileContent);
            
            
            
            ref array<string> meta_arr;
            
            ref array<string> sign1;
            ref array<string> sign2;
            ref array<string> sign3;
            ref array<string> signd1;
            ref array<string> signd2;
            ref array<string> signd3;
            ref array<string> signers_line_arr;
            string signers_line;
            
            sign1 = DZR_Notes().Explode("<signers values='",decoded_html.Get(1));
            sign2 = DZR_Notes().Explode("' />",sign1.Get(1));
            signers = DZR_Notes().Explode(",",sign2.Get(0), true);
            
            signd1 = DZR_Notes().Explode("<signers_dates values='",decoded_html.Get(1));
            signd2 = DZR_Notes().Explode("' />",signd1.Get(1));
            signers_dates = DZR_Notes().Explode(",",signd2.Get(0), true);
            
            Print(fileContent);
            Print(sign1);
            Print(sign2);
            Print(signers);
            Print(signd1);
            Print(signd2);
            Print(signers_dates);
        }
        else
        {
            Print("NOT FOUND:" + filePath);
        }
        return decoded_html.Get(0);
    }
    
    array<string> PackText(string text)
    {
        array<string> strs = new array<string>;
        string strSub;
        //Print("Start packing:" + text);
        int BufferSize = 256;
        int strLen = text.LengthUtf8();
        //Print("text.LengthUtf8()" + strLen);
        int strStart = 0;
        int strEnd = BufferSize; //256
        if(strLen > BufferSize)
        {
            while (strLen >= BufferSize)
            {
                strSub = text.SubstringUtf8(strStart, BufferSize); //0-256
                strLen = strSub.LengthUtf8(); // 256
                Print("\r\n\r\nPackText DayZGame: Adding string from "+strStart+" to "+strEnd+" ("+strLen+" ch): " + strSub);
                strStart = strStart + BufferSize; // 256
                strEnd = strEnd + BufferSize; //512
                strs.Insert(strSub);
            }
        }
        else
        {
            strs.Insert(text);
        }
        Print(strs);
        return(strs);
    }
    
    static string UnpackText(array<string> text)
    {
        string uncompressed;
        for ( int i = 0; i < text.Count(); i++ )
        {
            uncompressed += text.Get(i);
        }
        return(uncompressed);
    }
    
    array<string> UnpackTextToArray(array<string> text, bool EnableDebug = false)
    {
        if(EnableDebug) {Print("Unpacking to array");};
        
        array<string> strs = new array<string>;
        for ( int i = 0; i < text.Count(); i++ )
        {
            strs.Insert(text.Get(i));
            if(EnableDebug) {Print("Unpacking : Inserting "+text.Get(i));};
        }
        
        if(EnableDebug) {Print("Returning");};
        return(strs);
    }
    
    string SubstringUtf8Inverted( string string_to_split, int position_start, int position_end )
    {
        string first_half = string_to_split.SubstringUtf8(0, position_start);
        string second_half = string_to_split.SubstringUtf8( position_end, string_to_split.LengthUtf8() - position_end );
        string result = first_half + second_half;
        return result;
    }
}

