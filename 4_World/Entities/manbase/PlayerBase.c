modded class PlayerBase
{
	//protected bool m_SFSearchSound;
	protected bool m_DZRnotes_DuctSound;
    string AllowNoteCopy;
    string NotesPerPlayer;
    string SignatureClickURLparam;
    string SignatureClickURL;
    string AdminNoteCharLimit;
    string PlayerNoteCharLimit;
    
	void PlayerBase()
	{
		RegisterNetSyncVariableBool("m_DZRnotes_DuctSound");
        GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_ReceiveClientsideSettings", this, SingleplayerExecutionType.Both );
    }
	
    void DZR_ReceiveClientsideSettings(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
	{
		Print("\r\n\r\n[dzr_notes] ::: DZR_ReceiveClientsideSettings Got RPC from server");
		ref Param6< string, string, string, string, string, string > data;
        if ( !ctx.Read( data ) ) return;
        
        
		if (type == CallType.Client)
		{
            AllowNoteCopy = data.param1;
            NotesPerPlayer = data.param2;
            SignatureClickURLparam = data.param3;
            SignatureClickURL = data.param4;
            AdminNoteCharLimit = data.param5;
            PlayerNoteCharLimit = data.param6;
            
            Print(AllowNoteCopy);
            Print(NotesPerPlayer);
            Print(SignatureClickURLparam);
            Print(SignatureClickURL);
            Print(AdminNoteCharLimit);
            Print(PlayerNoteCharLimit);
        }
        
		
    }
    
	override void OnVariablesSynchronized()
	{
		super.OnVariablesSynchronized();		
		if (m_DZRnotes_DuctSound)
		{
			DZRDuctSoundItemSoundPlay();
        }
        
    }
	
	void DZRDuctSoundItemSoundPlay()
	{
        //int soundVariation = Math.RandomInt(5,8);
		EffectSound m_DZRDuctSound = SEffectManager.PlaySoundOnObject( "ducttape_tieup_SoundSet", this );
		//Print ("SOUND WHISTLE");
        m_DZRDuctSound.SetSoundVolume(2.0);
		m_DZRDuctSound.SetSoundAutodestroy( true );
		ResetDZRDuctSound();
    }
	
	void StartDZRDuctSound()
	{
		m_DZRnotes_DuctSound = true;
		SetSynchDirty();	
    }
	
	void ResetDZRDuctSound()
	{
		m_DZRnotes_DuctSound = false;
		SetSynchDirty();
    }
	
}