class DZR_PaperWritten extends Paper
{	
    static ref map<DZR_PaperWritten, string> m_AllWrittenNotes = new map<DZR_PaperWritten, string>();
    
	protected bool m_DZR_IsLocked;
	string m_DZR_NoteAuthor = "OLD_NOTE_";
	string m_DZR_NoteTitle;
	string m_DZR_NoteDate;
    string m_NotePID;
    int charLimit = 90;
    // TODO: Make configurable
    
	//protected string m_DZR_ItemNote;
	
    void DZR_PaperWritten()
	{
        RegisterNetSyncVariableBool("m_DZR_IsLocked");
        
        
        Print("\r\n\r\n========== Written note initiated ");
        /*
            if (GetGame().IsServer())
            {
            int b1, b2, b3, b4;
            string notePid;
            EntityAI target_object = this ;
            if(target_object)
            {
            target_object.GetPersistentID(b1, b2, b3, b4);
            }
            
            m_NotePID = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
            
            Print("\r\n\r\n========== Written note is trying to register pid: "+m_NotePID);
            DZR_Notes().RegisterNewNote(m_NotePID, target_object);
            //RegisterNetSyncVariableBool("m_DZR_ItemNote");
            // m_DZR_IsLocked = false;
            // m_DZR_ItemNote = "";
            }
        */
    }
    
    string GetTitle()
    {
        return m_AllWrittenNotes.Get(this);
    }
    
    override void EEDelete(EntityAI parent)
    {
        Print("\r\n----------------EEDelete DELETION:");
        GetPid();
        Print(this);
        Print(m_NotePID);
        float ltime = GetLifetime();
        Print(ltime);
        super.EEDelete(parent);
    };
    
    void GetPid()
    {
        int b1, b2, b3, b4;
        string notePid;
        
        if(this)
        {
            this.GetPersistentID(b1, b2, b3, b4);
        }
        
        m_NotePID = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
        
        //Print("\r\n\r\n========== Note pid: "+pid);
        Print("========== Note Pid: "+m_NotePID);
    }
    
	void SetPaperWrittenText(WrittenNoteData data)
	{
		m_NoteContents = data;
		// send server RPC
		
		//LOG NOTE
		/*
            ref Param1<string> m_Data = new Param1<string>(param1.param1);
            PlayerIdentity identity = sender;
            GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionServer_LogNote", m_Data, true, identity);
        */
        //LOG NOTE
        DZR_MissionServer_CheckFolders();
        vector thepos = this.GetPosition();
        string pName = "ERROR_no_player_Name";
        string pId = "ERROR_no_Steam_ID";
        
        pName = m_NoteContents.CreatedBy;
        pId = m_NoteContents.CreatedBySteamId;
        
        SaveServerSide(pName , pId, m_NoteContents.GetNoteText()  , thepos[0].ToString()+" "+thepos[1].ToString()+" "+thepos[2].ToString());
        
    }
    
    
    
    void SetNotePID(string PID)
	{
		m_NotePID = PID;
    }
	
	void SaveServerSide(string PlayerName, string steam_id, string Notetext, string ps = "")
	{
		int hour;
        int minute;
        int second;
        int year;
        int month;
        int day;
        GetHourMinuteSecond(hour, minute, second);
        GetYearMonthDay(year, month, day);
        string suffix = year.ToString()+"-"+month.ToString()+"-"+day.ToString()+"_"+hour.ToString()+"_"+minute.ToString()+"_"+second.ToString();
        string m_ObjectFileName = "$profile:DZR/dzr_notes/log/"+steam_id+"_"+suffix+".txt";
        //FileHandle fhandle;
        if ( !FileExist(m_ObjectFileName) )
        {
            
            FileHandle fhandle = OpenFile(m_ObjectFileName, FileMode.WRITE);
            FPrintln(fhandle, Notetext );
            FPrintln(fhandle, "_________" );
            FPrintln(fhandle, PlayerName+" (Steam ID: "+steam_id+")" );
            FPrintln(fhandle, this.ToString()+" ("+ps+")" );
            //FPrintln(fhandle, );
            CloseFile(fhandle);
            Print("DZR_PaperWritten.c ::: "+PlayerName + " ("+steam_id+") wrote text: " +Notetext+ " (logged to file: "+m_ObjectFileName+")");
        };
        
        /*
            
            string strSub = str.SubstringUtf8(0, 90);
            Print(strSub);
            
            >> strSub = にちは世
        */
        
        int strLen = Notetext.LengthUtf8();
        string title_suffix;
        if(strLen > charLimit)
        {
            title_suffix = "...";
        }
        string strSub = Notetext.SubstringUtf8(0, 90);
        m_DZR_NoteTitle = strSub + title_suffix;
        Print("\r\n");
        Print(m_DZR_NoteTitle);
        Print("\r\n");
        m_DZR_NoteAuthor = steam_id;
        
        m_AllWrittenNotes.Set(this, m_DZR_NoteTitle);
        
        //m_DZR_NoteText = Notetext+"\r\n--- "+PlayerName+"\r\n"+suffix;
        m_DZR_NoteDate = suffix;
        Param2<string, DZR_PaperWritten> data = new Param2<string, DZR_PaperWritten>(m_DZR_NoteTitle, this);
        GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_UpdateNoteTitleOnClient", data, true);
        
        //DuplicateNoteOnDisk();
    }
    
	void DZR_MissionServer_CheckFolders()
	{
		string dzr_Dzrnotes_ProfileFolder = "$profile:\\";
		string dzr_Dzrnotes_TagFolder = "DZR\\";
		string dzr_Dzrnotes_ModFolder = "dzr_notes\\";
		string dzr_Dzrnotes_SubFolder = "log\\";
		string dzr_Dzrnotes_SubFolder2 = "db_spawned_notes\\";
		
		//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile");
		if (!FileExist(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder)){
			//NO DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO DZR");
			MakeDirectory(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder);	
			MakeDirectory(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder+dzr_Dzrnotes_ModFolder);	
			//dzr_writeConfigFile();			
        }
		else 
		{
			//YES DZR
			//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile YES DZR");
			if (!FileExist(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder+dzr_Dzrnotes_ModFolder)){
				//NO IDZ
				//Print("[DZR IdentityZ] ::: DZR_MissionServer_ReadConfigFile NO IDZ");
				
				MakeDirectory(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder+dzr_Dzrnotes_ModFolder);
				
				
				//dzr_writeConfigFile();
            }
			else 
			{
				if (!FileExist(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder+dzr_Dzrnotes_ModFolder+dzr_Dzrnotes_SubFolder)){
					MakeDirectory(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder+dzr_Dzrnotes_ModFolder+dzr_Dzrnotes_SubFolder);
                }
				/*
                    if (!FileExist(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder+dzr_Dzrnotes_ModFolder+dzr_Dzrnotes_SubFolder2)){
					MakeDirectory(dzr_Dzrnotes_ProfileFolder+dzr_Dzrnotes_TagFolder+dzr_Dzrnotes_ModFolder+dzr_Dzrnotes_SubFolder2);
                    }
                */
            }
			
        };
		
		
		
		//PROPER CONFIG
		
    }
	
	override bool IsReadable()
	{
		return true;
    }
	
	override bool IsReadOnly()
	{
		return false;
    }
	
	override void SetActions()
	{
		AddAction(ActionPlaceObject);
		AddAction(ActionReadNoteOnGround);
		super.SetActions();
		AddAction(ActionWritePaper);
		AddAction(ActionReadPaper);
		AddAction(ActionTogglePlaceObject);
		
    }
	
	override bool DisableVicinityIcon()
	{
        
		return super.DisableVicinityIcon() + m_DZR_IsLocked;
    };
	
	void DZR_SetLocked(bool d_flag)
	{
        
		m_DZR_IsLocked = d_flag;
        SetSynchDirty();
    };
	
	void DZR_SetItemNote(string dnote)
	{
		//m_DZR_ItemNote = dnote;
    };
	
	
	
    override void OnStoreSave( ParamsWriteContext ctx )
    {
        m_NoteContents.SetNoteText("DEPRECATED");
		super.OnStoreSave( ctx );
        ctx.Write( m_DZR_IsLocked );
        ctx.Write( m_DZR_NoteAuthor );
        ctx.Write( m_DZR_NoteTitle );
        
        //force reduce value
        //m_DZR_NoteText = "DEPRECATED";
        
        //ctx.Write( m_DZR_NoteText );
        ctx.Write( m_DZR_NoteDate );
        DZR_Notes().SetStorageVersion(1);
        
        
        
        //ctx.Write( m_DZR_ItemNote );
        
        //Print("===========Trying to SAVE locked state============");
        //Print(this);
        // Print("OnStoreSave m_DZR_IsLocked ("+m_DZR_IsLocked+") lifetime: "+GetLifetime());
        //Print("OnStoreSave m_DZR_ItemNote ("+m_DZR_ItemNote+")");
        //Print(ctx);
		
        
    }
	
	
    override void AfterStoreLoad() {
        super.AfterStoreLoad();
		
        if (GetGame().IsServer() && GetGame().IsMultiplayer()) {
            // Print("===========Trying to RESTORE locked state============");
            // Print(this);
            // Print("AfterStoreLoad m_DZR_IsLocked ("+m_DZR_IsLocked+") lifetime: "+GetLifetime());
			if (m_DZR_IsLocked)
			{
				SetLifetime(3888000);
				SetAllowDamage(false);
				SetTakeable( false );
				//DZR_SetLocked(true);
				//DZR_SetItemNote(m_DZR_ItemNote);
				SetSynchDirty();
				// Print("AfterStoreLoad SetSynchDirty m_DZR_IsLocked ("+m_DZR_IsLocked+") NEW lifetime: "+GetLifetime());
				//Print("AfterStoreLoad SetSynchDirty m_DZR_ItemNote ("+m_DZR_ItemNote+")");
            }
            //DuplicateNoteOnDisk();
        }
    }
	
    void DuplicateNoteOnDisk()
    {
        string m_DZR_NoteDate_t;
        int hour;
        int minute;
        int second;
        int year;
        int month;
        int day;
        GetHourMinuteSecond(hour, minute, second);
        GetYearMonthDay(year, month, day);
        if(m_DZR_NoteDate == "")
        {
            m_DZR_NoteDate = year.ToString()+"-"+month.ToString()+"-"+day.ToString()+"_"+hour.ToString()+"_"+minute.ToString()+"_"+second.ToString();
        }
        m_DZR_NoteDate_t = day.ToString()+"."+month.ToString()+"."+year.ToString()+" "+hour.ToString()+":"+minute.ToString()+":"+second.ToString();
        
        if(m_DZR_NoteAuthor == "")
        {
            m_DZR_NoteAuthor = "OLD_NOTE_NO_AUTHOR";
        }
        string m_ObjectFileName = "$profile:DZR/dzr_notes/db_spawned_notes/"+m_DZR_NoteAuthor+"_"+m_DZR_NoteDate+".txt";
		//FileHandle fhandle;
        
		if ( !FileExist(m_ObjectFileName) )
		{   
            //ItemBase thePaper = ItemBase.Cast(this);
            if(this)
            {
                string m_DZR_NoteText = this.GetWrittenNoteData().GetNoteText();
            }
			FileHandle fhandle = OpenFile(m_ObjectFileName, FileMode.WRITE);
            vector coordsv = this.GetPosition();
            string coords = coordsv[0].ToString()+" "+coordsv[1].ToString()+" "+coordsv[2].ToString();
			FPrintln(fhandle, m_DZR_NoteText+"\r\n\r\n  SteamID: "+m_DZR_NoteAuthor+"\r\n  "+m_DZR_NoteDate_t+"\r\n  "+coords );
			CloseFile(fhandle);
			Print("\r\nDZR_PaperWritten.c ::: Spawned note of player "+m_DZR_NoteAuthor + "\r\n\    text: " +m_DZR_NoteText);
        }
    }
    
    override bool OnStoreLoad( ParamsReadContext ctx, int version )
    {
        Print("\r\n\r\n ============DZR_PaperWritten.c OnStoreLoad==========");
        Print(version);
        int dzr_version = DZR_Notes().GetStorageVersion();
        
        if ( super.OnStoreLoad( ctx, version ) )
        {
            
            if ( !ctx.Read( m_DZR_IsLocked ) ) return false;
            if ( !ctx.Read( m_DZR_NoteAuthor ) ) return false;
            if ( !ctx.Read( m_DZR_NoteTitle ) ) return false;
            //if ( !ctx.Read( m_DZR_NoteText ) ) return false;
            if ( !ctx.Read( m_DZR_NoteDate ) ) return false;
            Print(m_DZR_NoteTitle);
            
            
            
            return true;
            
        }
        
        return false;
        
        super.OnStoreLoad( ctx, version );
    }
	
};

class DZR_AdminPen extends Pen_ColorBase
{
    
    override bool IsReadOnly()
	{
		return true;
    }
	
	override string Result()
	{
		return "DZR_PaperWrittenAdmin";
    }
    
};

class DZR_AdminPen_Red extends Pen_ColorBase
{
    
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Red";
    }
    
};

class DZR_AdminPen_Green extends Pen_ColorBase
{
    
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Green";
    }
    
};

class DZR_AdminPen_Blue extends Pen_ColorBase
{
    
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Blue";
    }
    
};

//static
class DZR_AdminPen_Static extends Pen_ColorBase
{
    
	override bool IsReadOnly()
	{
		return true;
    }
	
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Static";
    }
    
};

class DZR_AdminPen_Red_Static extends DZR_AdminPen_Static
{
    
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Red_Static";
    }
    
};

class DZR_AdminPen_Green_Static extends DZR_AdminPen_Static
{
    
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Green_Static";
    }
    
};

class DZR_AdminPen_Blue_Static extends DZR_AdminPen_Static
{
    
	override string Result()
	{
		return "DZR_PaperWrittenAdmin_Blue_Static";
    }
    
};

class DZR_PaperWrittenAdmin extends DZR_PaperWritten
{
	override bool IsReadOnly()
	{
		return true;
    }
	
	override void SetActions()
	{
		super.SetActions();
		RemoveAction(ActionWritePaper);
    }
}		

class DZR_PaperWrittenAdmin_Red extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Green extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Blue extends DZR_PaperWrittenAdmin
{
	
}
//static

class DZR_PaperWrittenAdmin_Static extends DZR_PaperWrittenAdmin
{
	
}

class DZR_PaperWrittenAdmin_Red_Static extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Green_Static extends DZR_PaperWrittenAdmin
{
	
}	

class DZR_PaperWrittenAdmin_Blue_Static extends DZR_PaperWrittenAdmin
{
	
}