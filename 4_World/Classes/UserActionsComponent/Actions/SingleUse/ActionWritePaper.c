modded class ActionWritePaper
{
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
        
        if(item)
        {
            Paper ItemPaper = Paper.Cast(item);
            Pen_ColorBase ItemPen = Pen_ColorBase.Cast(item);
            //Print(ItemPaper.GetType()+": "+ItemPaper.IsReadOnly());
            
            if ( ItemPaper && ItemPaper.IsReadOnly() && !ItemPen.IsKindOf("DZR_AdminPen") )
            {
                return false;
            };
            
            if ( ItemPen )
            {
                ItemPaper = Paper.Cast(target.GetObject());
                if(ItemPaper && ItemPaper.IsReadOnly() && !ItemPen.IsKindOf("DZR_AdminPen"))
                {
                    return false;
                };
            };
        };
        return super.ActionCondition(  player,  target,  item );
    };
}

modded class ActionWritePaperCB
{
    override void OnStateChange(int pOldState, int pCurrentState)
	{
		if (pCurrentState == STATE_NONE && (!GetGame().IsDedicatedServer()))
		{
			if (GetGame().GetUIManager() && GetGame().GetUIManager().IsMenuOpen(MENU_NOTE))
            GetGame().GetUIManager().FindMenu(MENU_NOTE).Close();
        }
		else if (pCurrentState == STATE_LOOP_LOOP && pOldState != STATE_LOOP_LOOP && (!GetGame().IsMultiplayer() || GetGame().IsServer()))
		{
			ItemBase paper_item;
            DZR_AdminPen admin_pen;
			if (m_ActionData.m_MainItem.ConfigIsExisting("writingColor"))
			{
				paper_item = ItemBase.Cast(m_ActionData.m_Target.GetObject());
                
            }
			else
			{
				paper_item = ItemBase.Cast(m_ActionData.m_MainItem);
            }
			
            if(m_ActionData.m_MainItem.IsKindOf("DZR_AdminPen"))
            {
                admin_pen = DZR_AdminPen.Cast(m_ActionData.m_MainItem);
            }
            
            int b1, b2, b3, b4;
            string notePid;
            EntityAI target_object = paper_item ;
            
            if(target_object)
            {
                target_object.GetPersistentID(b1, b2, b3, b4);
            }
            
            notePid = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
            //del
            
            if(notePid == "0000")
            {
                Print("NO PID");
            };
            //getting PID
            Print("\t\nPID:");
            Print(notePid);
            JsonSaveData FileData = new JsonSaveData();
            if(admin_pen)
            {
                FileData = SendFileToClient("$profile:DZR\\dzr_notes\\notes_db\\static_admin\\"+notePid+".html");
            }
            else
            {
                FileData = SendFileToClient("$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid+".html");
            }
            Param2<ref JsonSaveData, string> text = new Param2<ref JsonSaveData, string>(FileData, notePid);
            
            
            paper_item.RPCSingleParam(ERPCs.RPC_WRITE_NOTE, text, true,m_ActionData.m_Player.GetIdentity());
        }
    }
    
    JsonSaveData SendFileToClient(string m_TxtFileName)
    {
        ref array<string> compressed = new array<string>;
        JsonSaveData packed_text_object = new JsonSaveData();
        
        // READ FILE CONTENTS
        string content;
        FileHandle fhandle;
        if (FileExist(m_TxtFileName))
        {
            Print("ActionWritePaper.c JsonSaveData SendFileToClient m_TxtFileName");
            Print(m_TxtFileName);
            fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
            string line_content;
            while ( FGets( fhandle,  line_content ) > 0 )
            {
                content += line_content;
            }
            CloseFile(fhandle);
        }
        else
        {
            Print("ActionWritePaper.c NO FILE! "+ m_TxtFileName);
        }
        // READ FILE CONTENTS
        compressed = DZR_Notes().PackText(content);
        packed_text_object.text = compressed;
        
        
        
        //save it
        //string m_SaveObjectFileName = "$profile:DZR/dzr_notes/pre_saved_text.json";
        //JsonFileLoader< JsonSaveData >.JsonSaveFile(m_SaveObjectFileName, importedConfig.text);
        //JsonFileLoader< array<string> >.JsonSaveFile(m_SaveObjectFileName, compressed);
        //save it
        
        return packed_text_object;
        //ref Param1<JsonSaveData> m_Data = new Param1<JsonSaveData>(packed_text_object);
        //GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", m_Data, true, identity);	
        //Print("[dzr_notes] ::: Sent RPC to client");
    }
};


