modded class MissionGameplay
{
	Widget m_dzr_layoutRoot; 
	MultilineEditBoxWidget m_edit;
	HtmlWidget m_html;
	ButtonWidget m_confirm_button;
    
	
    string m_CurrentPreview = "";
    
    Object m_CursorObject;
    Object m_previous_CursorObject;
    string m_PreviousPreview = "0";
    
    int note_RED_COLOR = ARGB(255, 255, 0, 0);
	int note_LIME_COLOR = ARGB(255, 0, 255, 0);
	int note_DEFAULT = ARGB(100, 255, 255, 255);
    bool Note_Raycasting = true;
    
    string updated_preview_pid;
    
    protected Widget m_DZR_notes_root;
	protected MultilineTextWidget m_dzrNotesCenterText;
    
	void MissionGameplay()
	{
		Print("[dzr_notes] ::: Starting Clientside");
        Note_Raycasting = true;
        GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_ShowNotePreview", this, SingleplayerExecutionType.Both );
        
        GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_UpdateNoteTitleOnClient", this, SingleplayerExecutionType.Both );
        GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_UpdateCurrentTitleOnCLient", this, SingleplayerExecutionType.Both );
        
		m_DZR_notes_root = GetGame().GetWorkspace().CreateWidgets( "czzzz_DzrNotesScripts/layouts/dzr_note_preview.layout" );
        m_dzrNotesCenterText = MultilineTextWidget.Cast( m_DZR_notes_root.FindAnyWidget("dzrCenterText2") );
        m_dzrNotesCenterText.SetText("DEFAULT TEXT");
        m_dzrNotesCenterText.Show(false);
		/*
            GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer", this, SingleplayerExecutionType.Both );
            m_dzr_layoutRoot = GetGame().GetWorkspace().CreateWidgets("dzr_notes/gui/layout/dzr_inventory_note.layout");
            m_edit = MultilineEditBoxWidget.Cast( m_dzr_layoutRoot.FindAnyWidget("EditWidget") );
            m_html = HtmlWidget.Cast( m_dzr_layoutRoot.FindAnyWidget("HtmlWidget") );
            m_confirm_button = ButtonWidget.Cast( m_dzr_layoutRoot.FindAnyWidgetById(IDC_OK) );
            m_dzr_layoutRoot.Show(false);
        */
        
        DZR_Notes().CheckFolders();
        
        
        
        
    };
	
    void DZR_UpdateCurrentTitleOnCLient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
    {
        Print("\r\n\r\n[dzr_notes] ::: DZR_UpdateCurrentTitleOnCLient Got RPC from server");
        
		ref Param2< string, string > data;
        if ( !ctx.Read( data ) ) return;
        
		if (type == CallType.Client)
		{
            //Print(this);
            //Print(data.param1);
            
            m_CurrentPreview = data.param1;
            updated_preview_pid = data.param2;
        }
    }
	
    void DZR_UpdateNoteTitleOnClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
    {
		Print("\r\n\r\n[dzr_notes] ::: DZR_UpdateNoteTitleOnClient Got RPC from server");
        
		ref Param2< string, DZR_PaperWritten > data;
        if ( !ctx.Read( data ) ) return;
        
		if (type == CallType.Client)
		{
            //Print(this);
            Print(data.param2);
            Print(data.param1);
            
            if(this == data.param2)
            {
                data.param2.m_DZR_NoteTitle = data.param1;
            }
        }
    }
    
	void DZR_MissionGameplay_ShowNotePreview(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
	{
		Print("\r\n\r\n[dzr_notes] ::: DZR_MissionGameplay_ShowNotePreview Got RPC from server");
		ref Param1< string > data;
        if ( !ctx.Read( data ) ) return;
        
        Print(data.param1)
		if (type == CallType.Client)
		{
            if(!Note_Raycasting)
            {
                Note_Raycasting = true;
                m_dzrNotesCenterText.SetText(data.param1);
                m_dzrNotesCenterText.Show(true);
            }
            
        }
        
		
    }
	
    void notes_RayCastForPlayer()
	{
        
        float VisibleDistance_m = 10;
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		
        if(player)
        {
            vector rayStart = GetGame().GetCurrentCameraPosition() + GetGame().GetCurrentCameraDirection() * 0.47;
            vector rayEnd = GetGame().GetCurrentCameraPosition() + GetGame().GetCurrentCameraDirection() * (VisibleDistance_m);		
            
            float distance;
            vector position;
            vector contact_dir;
            int contact_component;
            ref set<Object> hitObjects = new set<Object>;
            // RaycastRV(vector begPos, vector endPos, out vector contactPos, out vector contactDir, out int contactComponent, /*out*/ set<Object> results = NULL, Object with = NULL, Object ignore = NULL,  bool sorted = false,  bool ground_only = false,  int iType = ObjIntersectView, float radius = 0.0, CollisionFlags flags = CollisionFlags.NEARESTCONTACT);
            
            DayZPhysics.RaycastRV(rayStart, rayEnd, position, contact_dir, contact_component, hitObjects, null, player, false, false, ObjIntersectFire, 0.01);
            
            Print(m_CurrentPreview);
            Print(m_PreviousPreview);
            Print(m_previous_CursorObject);
            Print(m_CursorObject);
            Print(Note_Raycasting);
            
            Print(hitObjects);
            Print(hitObjects.Count());
            if ( hitObjects.Count() > 0 && GetGame().ObjectIsKindOf(hitObjects.Get(0),"DZR_PaperWritten"))
            {
                m_CursorObject = hitObjects.Get(0);
                Note_Raycasting = true;
            } 
            else 
            {
                m_CursorObject = NULL;
                //m_previous_CursorObject = NULL;
                m_CurrentPreview = "";
                m_PreviousPreview = "0";
                m_dzrNotesCenterText.Show(false);
            }
            
            // Print(m_CursorObject);
            
            if(Note_Raycasting && m_CursorObject && m_CurrentPreview != m_PreviousPreview)
            {
                // Print("\r\n\r\n\RAYCASTING======m_CursorObject==========");
                // Print(m_CursorObject);
                
                
                
                if(m_CursorObject != NULL )
                {
                    
                    
                    
                    m_previous_CursorObject = m_CursorObject;
                    m_PreviousPreview = m_CurrentPreview;
                    // Print("RAYCASTING======DZR_PaperWritten==========");
                    // Print(m_CursorObject);
                    // Print(the_note);
                    // Print(Note_Raycasting);
                    DZR_PaperWritten the_note  = DZR_PaperWritten.Cast(m_CursorObject);
                    if(the_note)
                    {
                        
                        // Note_Raycasting = true;
                        Print("RAYCASTING======the_note==========");
                        
                        
                        // Print(the_note);
                        // Print(the_note.m_DZR_NoteAuthor);
                        Print(the_note.m_DZR_NoteTitle);
                        // Print("RAYCASTING======the_note==========\r\n");
                        
                        string new_title = the_note.GetTitle();
                        Param1<DZR_PaperWritten> data = new Param1<DZR_PaperWritten>(the_note);
                        
                        ///PID
                        int b1, b2, b3, b4;
                        string notePid_mg;
                        EntityAI target_object = the_note ;
                        if(target_object)
                        {
                            target_object.GetPersistentID(b1, b2, b3, b4);
                        }
                        
                        notePid_mg = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
                        Print(notePid_mg);
                        ///PID
                        
                        GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_SyncPreviewOnCLient", data, true, player.GetIdentity());
                        distance = vector.Distance(player.GetPosition(), the_note.GetPosition());
                        
                        if(distance < 2.2)
                        {
                            //m_dzrNotesCenterText.SetText(the_note.m_NotePID+" ("+distance+")"+m_CursorObject.GetType()+":"+"\r\n["+m_CurrentPreview+"]");
                            m_dzrNotesCenterText.SetText(m_CurrentPreview);
                            m_dzrNotesCenterText.Show(true);
                            Note_Raycasting = false;
                            
                        }
                        else
                        {
                            m_dzrNotesCenterText.Show(false);
                            //m_CurrentPreview = ""; 
                        }
                        
                    }
                    else
                    {
                        the_note = null;
                        Print("NOT A NOTE?");
                    }
                    
                }
                else
                {
                    m_dzrNotesCenterText.Show(false);
                    //m_CurrentPreview = "";
                    Note_Raycasting = true;
                    //m_previous_CursorObject = NULL;
                    m_PreviousPreview = "0";
                }
                
                // m_CursorObject = NULL;
            }                    
            
        }
    }
    
    override void OnUpdate(float timeslice)
    {
        int delay = 1;
        float counter;
        
        this.notes_RayCastForPlayer();
        
        super.OnUpdate( timeslice );
    }
    
    
    Widget GetLayourutRoot()
    {
        return m_dzr_layoutRoot;
    }
    
    protected void DZR_NOTES_SaveToClientJson(ref JsonSaveData text)
    {
        
        GetGame().Chat("AddWriting", "colorImportant" );
        Print("AddWriting:" + text);
        //ref JsonSaveData save_data = new JsonSaveData();
        
        
        
        /*
            for ( int i = 0; i < arr1.Count(); i++ )
            {
            Print( arr1.Get(i) );
            }
        */
        
        //JsonFileLoader<array<string>>.JsonSaveFile(m_ObjectFileName, text);
        
        //ref array<string> decompressed = new array<string>;
        //decompressed.Insert(GetDayZGame().UnpackText(text));
        //destFile = "$profile:DZR/dzr_notes/decompressed_text.json";
        string m_ObjectFileName = "$profile:DZR/dzr_notes/json_from_server.json";
        JsonFileLoader<JsonSaveData>.JsonSaveFile(m_ObjectFileName, text);
        Print("[dzr_notes] ::: Written file");
        //JsonFileLoader< array< ref array<string> > >.JsonSaveFile(m_ObjectFileName, text[]);
        
    }	
};