class TransferTextToPaperWritten : ReplaceItemWithNewLambdaBase
{
    protected ref WrittenNoteData m_NoteContents;
    EntityAI m_NewNote;
    string m_CreatedBy;
    string m_NoteTitle;
    string m_CreatedBySteamID;
	ref array<string> packedContent;
    
    int att_count = 0; //CHANGE IN WRITTEN NOTE TOO 
	
    void TransferTextToPaperWritten(EntityAI old_item, string new_item_type, ref WrittenNoteData data, ref array<string> packedData, string CreatedBy, string CretedBySteamID, string NoteTitle)
    {
        
        Print("TransferTextToPaperWritten");
        Print(CreatedBy);
        Print(CretedBySteamID);
        Print(m_NoteTitle);
        Print(packedData);
        //m_NoteContents = data;
        m_NoteContents = data;
        m_CreatedBy = CreatedBy;
        m_CreatedBySteamID = CretedBySteamID;
        m_NoteTitle = NoteTitle;
        packedContent = packedData;
    }
	
	override void CopyOldPropertiesToNew(notnull EntityAI old_item, EntityAI new_item)
	{
		super.CopyOldPropertiesToNew(old_item, new_item);
        
		DZR_PaperWritten note = DZR_PaperWritten.Cast(new_item);
        if(note)
        {
            note.SetPaperWrittenText(m_NoteContents);
            note.SetPosition( old_item.GetPosition() );
            note.SetOrientation( old_item.GetOrientation() );
            note.m_DZR_NoteTitle = m_NoteTitle;
        }
		
    }
	
	override protected void OnSuccess(EntityAI new_item)
	{
		hndDebugPrint("[inv] ReplaceItemWithNewLambdaBase Step H) OnSuccess=" + new_item);
        Print("\r\n\r\n -------------- NEW NOTE");
        Print(new_item);
        ref array<string> m_ArrayFromFile_OnSuccess = new array<string>;		
        //getting PID
        string notePid4;
        int b1, b2, b3, b4;
        EntityAI target_object = new_item;
        //Print("EntityAI.Cast(m_Paper): "+target_object);
        //Print("m_Paper: "+m_Paper);
        if(target_object)
        {
            target_object.GetPersistentID(b1, b2, b3, b4);
        }
        notePid4 = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
        //Print("WrittenNoteData.c sending lambda.m_NewNote pid: "+notePid4);
        if(notePid4 == "0000")
        {
            //Print("WrittenNoteData.c: NO PID");
        };
        //getting PID
        //saveing to html locally
        
        m_ArrayFromFile_OnSuccess = DZR_Notes().UnpackTextToArray(packedContent, true);
        Print("OnSuccess:::: UnpackTextToArray");
        Print(m_ArrayFromFile_OnSuccess);
        //string unpackedText = DZR_Notes().UnpackText(packedContent);
        string filePath;
        if(new_item.IsKindOf("DZR_PaperWrittenAdmin_Static"))
        {
            filePath = "$profile:DZR\\dzr_notes\\notes_db\\static_admin\\"+notePid4+".html";
        }
        else
        {
            filePath = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid4+".html";
        }
        FileHandle file = OpenFile(filePath, FileMode.WRITE);
        string text;
        if ( file )
        {
            //Print("WrittenNoteData.c RPC_WRITE_NOTE_CLIENT : println to file " + filePath);
            //Print("WrittenNoteData.c RPC_WRITE_NOTE_CLIENT m_ArrayFromFile.Count() " + m_ArrayFromFile.Count());
            FPrintln(file, "<!DOCTYPE html>");
            FPrintln(file, "	<html>");
            FPrintln(file, "	<head>");
            FPrintln(file, "		<meta http-equiv='content-type' content='text/html; charset=UTF-8'/>");
            FPrintln(file, "		<dzr_meta>");
            FPrintln(file, "		<author values='"+m_CreatedBy+";"+m_CreatedBySteamID+"' />"); //+";"+unp_Date+";"+unp_Sign+"\" />");
            FPrintln(file, "		</dzr_meta>");
            FPrintln(file, "	</head>");
            FPrintln(file, "	<body>");
            
            /*
                
                Центральный банк в эту пятницу довольно неожиданно для многих оставил ключевую ставку на уровне 21%. Спросили у экспертов, как такое решение скажется на рубле.
                
                — Для рубля ставка ЦБ сейчас имеет слабое влияние, как было до 2022 года, — арбитраж невозможен. Но есть иные факторы поддержки курса — положительный торговый баланс, восстановление механизмов расчетов за экспорт, ненулевая вероятность геополитического послабления и снижения риск-премии. По нашим оценкам, доллар у 100, а юань под 14 — это адекватные уровни на конец 2024-го — начало 2025 года, — пояснили корреспонденту 72.RU в «БКС Мир инвестиций».
                
                Реклама
                
            */
            
            
            
            //FPrintln(file, "		<attributes values=\""+unp_IsCopy+";"+unp_canCopy+";"+unp_TitleColor+";"+unp_TextColor+";"+unp_Completed+"\"  />");
            text.Replace("\r", "");
            text.Replace("\n", "");
            FPrintln(file, "        <p>");
            for (int i = 0; i < m_ArrayFromFile_OnSuccess.Count(); i++; )
            {
                text +=  m_ArrayFromFile_OnSuccess.Get(i);
                Print("\r\n\r\nADDING LINE:");
                Print(m_ArrayFromFile_OnSuccess.Get(i));
                FPrint(file, m_ArrayFromFile_OnSuccess.Get(i));
            };
            FPrintln(file, "</p>");
            FPrintln(file, "	</body>");
            FPrintln(file, "</html>");
            
            //Print("FPrintln(file, text):" + text);
            CloseFile(file);
        }
        else
        {
            Print("WrittenNoteData.c RPC_WRITE_NOTE_CLIENT : not found " + filePath);
        };	
		
    }
}