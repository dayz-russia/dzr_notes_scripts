modded class NoteMenu extends UIScriptedMenu
{
	
    string SignedSteamID;
    string SignedUserName;
    string DateSigned;
    
    string empty_template = "$profile:DZR\\dzr_notes\\notes_db\\personal\\empty_template.html";
    
    TextWidget m_signature;
    TextWidget m_info;
    TextWidget m_warn;
    TextWidget m_signDate;
    ButtonWidget m_signerButton;
	CheckBoxWidget m_sign_cb;
	TextWidget m_signedby;
    
    PlayerBase player;
    
    int note_limit_view;
    int note_limit_edit;
    int chars_offset = 289;
    int EditFieldLimit = 1690;
    
    string AllowNoteCopy;
    string NotesPerPlayer;
    string SignatureClickURLparam;
    string SignatureClickURL;
    string AdminNoteCharLimit;
    string PlayerNoteCharLimit;
    
    
    
    ref array<string> decoded_html;
    
	void ~NoteMenu()
	{
		MissionGameplay mission = MissionGameplay.Cast( GetGame().GetMission() );
		if( mission )
		{
			IngameHud hud = IngameHud.Cast( mission.GetHud() );
			if ( hud )
			{
				hud.ShowHudUI( true );
            }
			GetGame().GetMission().RemoveActiveInputExcludes({"inventory"},false);
			GetGame().GetMission().RemoveActiveInputRestriction(EInputRestrictors.INVENTORY);
        }
    }
	
	override Widget Init()
	{
        
        player = PlayerBase.Cast( GetGame().GetPlayer() );
        
        AllowNoteCopy = player.AllowNoteCopy;
        NotesPerPlayer = player.NotesPerPlayer;
        SignatureClickURLparam = player.SignatureClickURLparam;
        SignatureClickURL = player.SignatureClickURL;
        AdminNoteCharLimit = player.AdminNoteCharLimit;
        PlayerNoteCharLimit = player.PlayerNoteCharLimit;
        
		//Print("[dzr_notes] ::: Started");
		//GetGame().Chat("[dzr_notes] ::: Widget Init", "colorAction");
		layoutRoot = GetGame().GetWorkspace().CreateWidgets("czzzz_DzrNotesScripts/layouts/new_note_ui.layout");
		m_edit = MultilineEditBoxWidget.Cast( layoutRoot.FindAnyWidget("dzrTextInput") );
		m_html = HtmlWidget.Cast( layoutRoot.FindAnyWidget("dzrTextView") );
		//m_html.LoadFile( "$profile:DZR/dzr_notes/temp/json_from_server2.json" );
		m_confirm_button = ButtonWidget.Cast( layoutRoot.FindAnyWidgetById(IDC_OK) );
		
        m_sign_cb = CheckBoxWidget.Cast( layoutRoot.FindAnyWidget("dzrSignCB") );
        m_signerButton = ButtonWidget.Cast( layoutRoot.FindAnyWidget("dzrSignerProfile") );
		m_signedby = TextWidget.Cast( layoutRoot.FindAnyWidget("dzrSignedBy") );
		m_signature = TextWidget.Cast( layoutRoot.FindAnyWidget("dzrSignature") );
		m_signDate = TextWidget.Cast( layoutRoot.FindAnyWidget("dzrSignDate") );
        
		m_warn = TextWidget.Cast( layoutRoot.FindAnyWidget("dzrWarn") );
		m_warn.Show(false);
		m_info = TextWidget.Cast( layoutRoot.FindAnyWidget("dzrInfo") );
		m_info.Show(false);
        
        
		return layoutRoot;
    }
	
    override void InitNoteWrite(EntityAI paper, EntityAI pen, string text = "")
	{
		m_IsWriting = true;
		
        Print("\r\nInitNoteWrite");
        
        Print(paper);
        Print(text);
        
        string filePath
        if (text.Contains("DZR_ADMIN_NOTE#"))
        {
            text.Replace("DZR_ADMIN_NOTE#","");
            filePath = "$profile:DZR\\dzr_notes\\notes_db\\static_admin\\"+text+".html";
        }
        else
        {
            filePath = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+text+".html";
        }
        //TODO add text formating here. Just text string for now
        string fileContent ="";
        if ( text != "" && FileExist( filePath ) )
        {
            
            FileHandle fhandle	=	OpenFile(filePath, FileMode.READ);		
            FGets( fhandle,  fileContent );
            CloseFile(fhandle);
            
        }
        
		if (m_edit)
		{
			m_Paper = ItemBase.Cast(paper);
            m_Pen = pen;
            m_PenColor = m_Pen.ConfigGetString("writingColor");
            if (m_PenColor == "")
            {
                m_PenColor = "#000000";
            }
            //m_Handwriting = handwriting;
            
            m_edit.Show(true);
            
            
            
            decoded_html = DZR_Notes().DecodeHtml(fileContent);
            Print("\r\n\r\n----------------------decoded_html.Get(0)");
            Print(decoded_html.Get(0));
            
            Print("\r\n\r\n----------------------fileContent.LengthUtf8()");
            Print(fileContent.LengthUtf8());
            
            
            m_edit.SetText("");
            m_edit.SetText(decoded_html.Get(0));
            SetFocus(m_edit);
            Print("m_edit.GetText()");
            string theeditttext;
            m_edit.GetText(theeditttext);
            Print(theeditttext);
            
            int curLen = fileContent.LengthUtf8() - 289;
            
            if(curLen >= EditFieldLimit)
            {
                m_warn.Show(true);
                
                //Print(m_edit.GetLinesCount());
                Print("\r\n\r\n     TEXT VALIDATION\r\n"+decoded_html.Get(0));
                
                Print("\r\n\r\n");
                
                
                m_warn.SetText("Text more than "+EditFieldLimit+" characters cannot be\r\nloaded in this field for editing.\r\nText length: " +curLen);
                //return false;
            };
            
            m_signature.Show(false);
            m_signedby.Show(false);
            m_signDate.Show(false);
            m_signerButton.Show(false);
            m_sign_cb.Show(true);
        }
		
		if (m_html)
		{
			m_html.Show(false);
        }
		m_confirm_button.Show(true);
    }
    
	override void InitNoteRead(string text = "ErrorPID")
	{
        
        Print("\r\n = ============ INIT NOTE READ")
		m_IsWriting = false;
		
		if (m_edit)
		{
			m_edit.Show(false);
        }
		
        ref array<string> signers = new array<string>();
        ref array<string> signers_dates = new array<string>();
        
		if (m_html)
		{
            /*
                Print(GetGame().IsServer());
                {
                Print("NoteMenu GetGame().IsClient()");
                Print(GetGame().IsClient());
                Print("NoteMenu GetGame().IsServer()");
                Print(GetGame().IsServer());
                }
                if(GetGame().IsClient())
                {
                Print("NoteMenu GetGame().IsClient()");
                Print(GetGame().IsClient());
                Print("NoteMenu GetGame().IsServer()");
                Print(GetGame().IsServer());
                }
            */
			string filePath = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+text+".html";
            
            
            
			//TODO add text formating here. Just text string for now
			if ( FileExist( filePath ) )
			{
				
                DZR_Notes().LoadNoteMeta(filePath, signers, signers_dates);
				Print("NoteMenu content file exists clintside, loading in " + filePath );
				m_html.Show(true);
				//m_signature.SetText("");
				//m_signedby.SetText("");
				//m_signDate.SetText("");
                float m_vsize;
                m_vsize = (m_html.GetNumLines() * 20) + 5;
                float hw;
                float hh;
                m_html.GetSize(hw,hh);
                m_html.SetSize(hw, m_vsize);
                
				m_html.SetText("");
				m_html.LoadFile( empty_template );
				m_html.LoadFile( filePath );
				
                
                
				//string getText = m_html.GetText();
                //Print(getText)
				//m_SymbolCount = text.Length(); //string.LengthUtf8() ?
				//m_html.SetText("<html><body><p>" + text + "</p></body></html>");
            }
        }
		m_confirm_button.Show(false);
        
        m_signature.Show(false);
        m_signedby.Show(false);
        m_signDate.Show(false);
        
        //TODO: Check signers meta
        if(signers.Count() > 0)
        {
            Print(signers);
            Print(signers.Get(0));
            Print(signers_dates);
            ref array<string> signers_name_arr;
            signers_name_arr = DZR_Notes().Explode(";", signers.Get(0), true);
            Print(signers.Count());
            Print(signers_name_arr);
            SignedSteamID = signers_name_arr.Get(1);
            m_signature.SetText(signers_name_arr.Get(0));
            m_signDate.SetText(signers_dates.Get(0));
            
            m_signature.Show(true);
            m_signedby.Show(true);
            m_signDate.Show(true);
            m_signerButton.Show(true);
        }
        m_sign_cb.Show(false);
        
    }
    
	override bool OnKeyPress(Widget w, int x, int y, int key)
	{
        
        ValidateText();
        
    	return super.OnKeyPress(w, x, y, key);
    }
    
    bool ValidateText()
    {
        string txt;
        m_edit.GetText(txt);
        if(txt.LengthUtf8() >= PlayerNoteCharLimit.ToInt())
        {
            
            
            //Print(m_edit.GetLinesCount());
            Print("\r\n\r\n     TEXT VALIDATION\r\n"+txt);
            
            
            Print(txt.LengthUtf8());
            Print("\r\n\r\n");
            
            m_info.Show(false);
            m_warn.Show(true);
            m_warn.SetText("#STR_DZR_Chars_Limit"+" "+PlayerNoteCharLimit);
            return false;
        }; 
        m_warn.Show(false);
        m_info.Show(true);
        int charLeft = PlayerNoteCharLimit.ToInt() - txt.LengthUtf8();
        m_info.SetText("Characters left: "+charLeft);
        
        return true;
    }
    
	override bool OnClick(Widget w, int x, int y, int button)
	{
		
		
        if(!ValidateText())    
        {
            return false;
        }
        
        
        
        if ( w == m_edit || w == layoutRoot)
        {
            SetFocus(m_edit);
        };
        
        if(SignatureClickURL != "0")
        {
            if ( w == m_signerButton && SignedSteamID != "")
            {
                
                if(SignatureClickURLparam == "player_name")
                {
                    GetGame().OpenURL( SignatureClickURL+player.GetIdentity().GetName() ); 
                };
                
                if(SignatureClickURLparam == "steam_id")
                {
                    GetGame().OpenURL( SignatureClickURL+player.GetIdentity().GetPlainId() ); 
                };
                
                if(SignatureClickURLparam == "0")
                {
                    GetGame().OpenURL( SignatureClickURL ); 
                };
            }
            else
            {
                return false;
            }
        };
        
        super.OnClick(w, x, y, button);
        switch (w.GetUserID())
        {
            case IDC_CANCEL:
            Close();
            return true;
            
            case IDC_OK:
            
            m_warn.Show(false);
            if (m_Paper && m_Pen && m_IsWriting)
            {
                
                
                
                
                string edit_text;
                string edit_text1;
                m_edit.GetText(edit_text);
                m_edit.GetText(edit_text1);
                //Print("edit_text (" + edit_text.Length() + ": UTF8 "+edit_text.LengthUtf8());
                //Print("edit_text1 (" + edit_text1.Length() + ": UTF8 "+edit_text1.LengthUtf8());
                //Print("edit_text: " + edit_text);
                //edit_text = MiscGameplayFunctions.SanitizeString(edit_text);
                //Print("SanitizeString edit_text: " + edit_text);
                Param1<string> text = new Param1<string>(edit_text);
                //pack here
                ref array<string> compressed = new array<string>();
                ref array<string> attributes = new array<string>();
                ref array<string> signers = new array<string>();
                ref array<string> signers_dates = new array<string>();
                ref array<string> consumed_objects = new array<string>();
                ref array<string> viewers = new array<string>();
                ref array<string> viewers_dates = new array<string>();
                ref array<string> whitelist_steamid = new array<string>();
                
                
                
                JsonSaveData packed_text_object = new JsonSaveData();
                
                packed_text_object.text = compressed;
                packed_text_object.attributes = attributes;
                packed_text_object.signers = signers;
                packed_text_object.signers_dates = signers_dates;
                packed_text_object.consumed_objects = consumed_objects;
                packed_text_object.viewers = viewers;
                packed_text_object.viewers_dates = viewers_dates;
                packed_text_object.whitelist_steamid = whitelist_steamid;
                
                //string testText = '{    "text": [        "TEST LOCAL TEXT"    ]			}';
                
                string m_ObjectFileName = "$profile:DZR/dzr_notes/latest_input.txt";        
                FileHandle fhandle = OpenFile(m_ObjectFileName, FileMode.WRITE);
                FPrint(fhandle, edit_text);
                
                /*
                    
                    Центральный банк в эту пятницу довольно неожиданно для многих оставил ключевую ставку на уровне 21%. Спросили у экспертов, как такое решение скажется на рубле.
                    
                    — Для рубля ставка ЦБ сейчас имеет слабое влияние, как было до 2022 года, — арбитраж невозможен. Но есть иные факторы поддержки курса — положительный торговый баланс, восстановление механизмов расчетов за экспорт, ненулевая вероятность геополитического послабления и снижения риск-премии. По нашим оценкам, доллар у 100, а юань под 14 — это адекватные уровни на конец 2024-го — начало 2025 года, — пояснили корреспонденту 72.RU в «БКС Мир инвестиций».
                    
                    --------
                    
                    
                    
                    
                */
                //setting Note Title
                int strLen = edit_text.LengthUtf8();
                string title_suffix;
                if(strLen > 90)
                {
                    title_suffix = "...";
                }
                string strSub = edit_text.SubstringUtf8(0, 90);
                string m_DZR_NoteTitle = strSub + title_suffix;
                packed_text_object.NoteTitle = m_DZR_NoteTitle;
                Print(m_DZR_NoteTitle);
                Print(packed_text_object.NoteTitle);
                //setting Note Title
                
                edit_text.Replace("\n", "<br/>");
                compressed = DZR_Notes().PackText(edit_text);
                
                //compressed = PackText(edit_text);
                packed_text_object.text = compressed;
                packed_text_object.CreatedBy = player.GetIdentity().GetName();
                packed_text_object.CreatedBySteamId = player.GetIdentity().GetPlainId();
                
                if(m_sign_cb.IsChecked())
                {
                    packed_text_object.signers.Insert(player.GetIdentity().GetName()+";"+player.GetIdentity().GetPlainId());
                    
                    int hour;
                    int minute;
                    int second;
                    int year;
                    int month;
                    int day;
                    GetHourMinuteSecond(hour, minute, second);
                    GetYearMonthDay(year, month, day);
                    string day_prefix = "";
                    string month_prefix = "";
                    string hour_prefix = "";
                    string minute_prefix = "";
                    
                    if(day < 10) {day_prefix = "0"};
                    if(month < 10) {month_prefix = "0"};
                    if(hour < 10) {hour_prefix = "0"};
                    if(minute < 10) {minute_prefix = "0"};
                    
                    string suffix = day_prefix+day.ToString()+"."+month_prefix+month.ToString()+"."+year.ToString()+" "+hour_prefix+hour.ToString()+":"+minute_prefix+minute.ToString(); //+":"+second.ToString();
                    
                    packed_text_object.signers_dates.Insert(suffix);
                    
                }
                
                packed_text_object.CreatedDate = "TODO";
                packed_text_object.ModifiedBy = "TODO";
                packed_text_object.ModifiedBySteamid = "TODO";
                packed_text_object.ModifiedByDate = "TODO";
                packed_text_object.CreatedDate = "TODO";
                //Print("NoteMenu.c packed_text_object: " + packed_text_object);
                //Print("NoteMenu.c packed_text_object.text: " + packed_text_object.text);
                //Print("NoteMenu.c packed_text_object.text[0]: " + packed_text_object.text[0]);
                //Print("NoteMenu.c packed_text_object.text[1]: " + packed_text_object.text[1]);
                //packed_text_object.text = testText;
                
                //notePid here
                
                
                //getting PID
                //m_Player
                int b1, b2, b3, b4;
                string notePid;
                //ItemBase paper_item = ItemBase.Cast( player.GetItemInHands() );
                //ItemBase paper_item = ItemBase.Cast( m_Paper );
                EntityAI target_object = m_Paper;
                if(target_object)
                {
                    target_object.GetPersistentID(b1, b2, b3, b4);
                }
                
                notePid = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
                //Print("NoteMenu.c sending pid: "+notePid);
                //del
                
                if(notePid == "0000")
                {
                    //Print("NoteMenu: NO PID");
                };
                //getting PID
                
                
                
                
                Param5<string, ref JsonSaveData, string, EntityAI, string> data = new Param5<string, ref JsonSaveData, string, EntityAI, string>("DEPRECATED", packed_text_object, notePid, m_Paper, m_DZR_NoteTitle);
                m_Paper.RPCSingleParam(ERPCs.RPC_WRITE_NOTE_CLIENT, data, true);
                
                
                
                
            }
            Close();
            return true;
        }
        
        return false;
    }
    /*
        
    */
    array<string> PackText(string text)
    {
        array<string> strs = new array<string>;
        string strSub;
        Print("\r\n\r\n\NoteMenu.c ====== Start packing:" + text);
        int BufferSize = 256;
        int strLen = text.LengthUtf8();
        Print("text.LengthUtf8()" + strLen);
        int strStart = 0;
        int strEnd = BufferSize; //256
        if(strLen > BufferSize)
        {
            while (strLen >= BufferSize)
            {
                strSub = text.SubstringUtf8(strStart, BufferSize); //0-256
                strLen = strSub.LengthUtf8(); // 256
                Print("PackText NoteMenu:Adding string from "+strStart+" to "+strEnd+" ("+strLen+" ch): " + strSub);
                strStart = strStart + BufferSize; // 256
                strEnd = strEnd + BufferSize; //512
                strs.Insert(strSub);
            }
        }
        else
        {
            strs.Insert(text);
        }
        return(strs);
    }
}
