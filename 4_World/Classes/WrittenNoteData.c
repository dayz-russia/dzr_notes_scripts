modded class WrittenNoteData
{
	protected ItemBase 	m_WritingImplement;
	protected ItemBase	m_Paper;
	protected PlayerBase	m_Player;
    protected string 	m_Pid;
	//protected int 		m_Handwriting = -1;
	protected string 	m_SimpleText;
	protected string 	m_TextFromFile;// = "default";
	string 	CreatedBy = "ERROR";// = "default";
	string 	NoteTitle = "ERROR";// = "default";
	string 	CreatedBySteamId = "ERROR";// = "default";
	protected JsonSaveData 	m_JsonTextFromFile;// = "default";
	
    int att_count = 0;// Change in text transfer too 
    
	void WrittenNoteData(ItemBase parent)
	{
		GetRPCManager().AddRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", this, SingleplayerExecutionType.Both );
    }
	
	void DZR_MissionGameplay_GetFileFromServer2(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender)
	{
		Print("[dzr_notes] ::: Got RPC from server");
		Param1<JsonSaveData> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{
            m_TextFromFile = DZR_Notes().UnpackText(data.param1.text);
            
            
            
            Print("[dzr_notes] ::: data.param1.text: "+data.param1.text);
            Print("[dzr_notes] ::: data.param1.text[0]: "+data.param1.text[0]);
            Print("[dzr_notes] ::: data.param1.text[1]: "+data.param1.text[1]);
            Print("[dzr_notes] ::: data.param1.text[2]: "+data.param1.text[2]);
            Print("[dzr_notes] ::: m_TextFromFile: "+m_TextFromFile);
            Print("[dzr_notes] ::: DZR_Notes().UnpackText(data.param1.text): "+DZR_Notes().UnpackText(data.param1.text));
            //Print("[dzr_notes] ::: DZR_Notes().UnpackText(data.param1.text[0]): "+DZR_Notes().UnpackText(data.param1.text[0]));
            //DZR_NOTES_SaveToClientJson(data.param1);
            string m_ObjectFileName = "$profile:DZR/dzr_notes/temp/json_from_server2.json";
            JsonFileLoader<JsonSaveData>.JsonSaveFile(m_ObjectFileName, data.param1);
            
            
        };
    };
	
	
	protected void DZR_NOTES_SaveToClientJson(ref JsonSaveData text)
	{
		
		GetGame().Chat("AddWriting", "colorImportant" );
		//Print("AddWriting:" + text);
		//ref JsonSaveData save_data = new JsonSaveData();
		
		
		
		/*
			for ( int i = 0; i < arr1.Count(); i++ )
			{
			Print( arr1.Get(i) );
			}
        */
		
		//JsonFileLoader<array<string>>.JsonSaveFile(m_ObjectFileName, text);
		
		//ref array<string> decompressed = new array<string>;
		//decompressed.Insert(GetDayZGame().UnpackText(text));
		//destFile = "$profile:DZR/dzr_notes/decompressed_text.json";
		string m_ObjectFileName = "$profile:DZR/dzr_notes/json_from_server.json";
		JsonFileLoader<JsonSaveData>.JsonSaveFile(m_ObjectFileName, text);
		//Print("[dzr_notes] WrittenNoteData.c ::: Written file");
		//JsonFileLoader< array< ref array<string> > >.JsonSaveFile(m_ObjectFileName, text[]);
		
    }	
	
	
	override void OnRPC( PlayerIdentity sender, int rpc_type, ParamsReadContext  ctx)
	{
		Param5<string, ref JsonSaveData, string, EntityAI, string> param;
        //Param1<string> param = new Param1<string>("");
		//m_Player = GetPlayerObjectByIdentity(sender);
		string notePid2;
		string notePid;
		ref array<string> m_ArrayFromFile = new array<string>;
		string line_content;
		ItemBase pen;
		ItemBase paper;
		Param2<ref JsonSaveData, string> param_RPC_WRITE_NOTE;
        string meta;
        
        array<string> strFileParam;
        array<string> strFileParam2;
        strFileParam = new array<string>;
        strFileParam2 = new array<string>;
        
        string brConverted;
		//sent from server, executed on client
        
		if (rpc_type == ERPCs.RPC_WRITE_NOTE) //CLIENTSIDE
		{
			ref array<string> m_ArrayFromFile_RPC_WRITE_NOTE = new array<string>;
            //m_Player = GetPlayerObjectByIdentity(sender);
            
            //Param1<string> param1 = new Param1<string>("");
            
            if (ctx.Read(param_RPC_WRITE_NOTE))
            {
                GetNoteInfo(pen,paper);
                if(paper.IsKindOf("DZR_PaperWritten"))
                {
                    notePid2 = param_RPC_WRITE_NOTE.param2;
                    
                    
                    ref array<string> m_ArrayFromFile_RPC_READ_NOTE2 = new array<string>;
                    m_ArrayFromFile_RPC_READ_NOTE2 = DZR_Notes().UnpackTextToArray(param_RPC_WRITE_NOTE.param1.text, false);
                    //Param2<ref JsonSaveData, string> param_RPC_READ_NOTE;
                    //load exisitng
                    string filePath12 = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid2+".html"; //CLIENT PATH
                    FileHandle file12 = OpenFile(filePath12, FileMode.WRITE);
                    string text12;
                    if ( file12 )
                    {
                        Print("WrittenNoteData.c ERPCs.RPC_EDIT_NOTE : DZRN : println to file filePath12 " + filePath12);
                        Print("WrittenNoteData.c ERPCs.RPC_EDIT_NOTE : m_ArrayFromFile_RPC_READ_NOTE2.Count() " + m_ArrayFromFile_RPC_READ_NOTE2.Count());
                        //FPrintln(file, "<body>");
                        for (int i12 = 0; i12 < m_ArrayFromFile_RPC_READ_NOTE2.Count(); i12++; )
                        {
                            text12 +=  m_ArrayFromFile_RPC_READ_NOTE2.Get(i12);
							FPrint(file12, m_ArrayFromFile_RPC_READ_NOTE2.Get(i12));
                        }
                        
                        //FPrintln(file, "</body>");
                        //Print("FPrintln(file, text):" + text);
                        CloseFile(file12);
                    }
                    else
                    {
                        Print("WrittenNoteData.c RPC_EDIT_NOTE filePath12 : not found " + filePath12);
                    };		
                    //saveing to html locally
                    
                    strFileParam = new array<string>;
                    strFileParam2 = new array<string>;
                    /*
                        strFileParam2 = new array<string>;
                        line_content = text3; 
                        line_content.Split( "<body>", strFileParam );
                        
                        Print("==========SPLIT======0=====");
                        Print(strFileParam.Get(0));
                        Print("================1=====");
                        Print(strFileParam.Get(1));
                        
                        strFileParam.Get(1).Split( "</body>", strFileParam2 );
                        
                        Print("==========SPLIT2=======0====");
                        Print(strFileParam2.Get(0));
                        Print("=================1====");
                        Print(strFileParam2.Get(1));
                    */
                    
                    //deHTML
                    text12.Replace("\r","");
                    text12.Replace("\n","");
                    text12.Replace("<!DOCTYPE html>","");
                    text12.Replace("<html>","");
                    text12.Replace("<head>","");
                    text12.Replace("</head>","");
                    text12.Replace("<meta http-equiv='content-type' content='text/html; charset=UTF-8'/>","");
                    //text12.Replace("<p>","");
                    text12.Replace("</p>","");
                    text12.Replace("</body>","");
                    text12.Replace("</html>","");
                    //deHTML
                    
                    text12.Split( "<p>", strFileParam ); //text
                    line_content = strFileParam2.Get(1);
                    brConverted = line_content;
                    
                    brConverted.Replace("<br/>","\n");
                    
                    strFileParam2.Get(0).Split( "<dzr_meta>", strFileParam2 );
                    meta = strFileParam2.Get(1);
                    meta.Replace("</dzr_meta>","");
                    Print(meta);
                    
                    line_content = brConverted.Trim();
                    //m_SimpleText = line_content;
                    //SetNoteText(line_content);
                    //converting from html
                    
                    Print(line_content);
                    //m_SimpleText = line_content;
                    //SetNoteText(line_content);
                    
                    g_Game.GetMission().SetNoteMenu(g_Game.GetUIManager().EnterScriptedMenu(MENU_NOTE, GetGame().GetUIManager().GetMenu()));
                    if (GetNoteInfo(pen,paper))
                    {
                        Print(paper);
                        if(pen.IsKindOf("DZR_AdminPen"))
                        {
                            g_Game.GetMission().GetNoteMenu().InitNoteWrite(paper,pen,"DZR_ADMIN_NOTE#"+line_content);
                        }
                        else
                        {
                            g_Game.GetMission().GetNoteMenu().InitNoteWrite(paper,pen,notePid2);
                        }
                        
                        
                        
                    }
                }
                else
                {
                    Print("\r\nparam_RPC_WRITE_NOTE: " + param_RPC_WRITE_NOTE);
                    Print("param_RPC_WRITE_NOTE.param1: " + param_RPC_WRITE_NOTE.param1);
                    /*
                        Print("param_RPC_WRITE_NOTE.param1.text: " + param_RPC_WRITE_NOTE.param1.text);
                        Print("param_RPC_WRITE_NOTE.param1.text[0]: " + param_RPC_WRITE_NOTE.param1.text[0]);
                        Print("param_RPC_WRITE_NOTE.param1.text[1]: " + param_RPC_WRITE_NOTE.param1.text[1]);
                        string m_ObjectFileName = "$profile:DZR/dzr_notes/temp/json_from_server2.json";
                        JsonFileLoader<JsonSaveData>.JsonSaveFile(m_ObjectFileName, param_RPC_WRITE_NOTE.param1);
                        m_JsonTextFromFile = param_RPC_WRITE_NOTE.param1;
                        m_TextFromFile = DZR_Notes().UnpackText(param_RPC_WRITE_NOTE.param1.text);
                        SetNoteText(m_TextFromFile);
                        
                        Print("m_TextFromFile: " + m_TextFromFile);
                        //SetNoteText(param_RPC_WRITE_NOTE.param1);
                    */
                    
                    
                    //getting PID
                    
                    notePid2 = param_RPC_WRITE_NOTE.param2;
                    //getting PID
                    
                    
                    //saveing to html locally
                    
                    m_ArrayFromFile_RPC_WRITE_NOTE = DZR_Notes().UnpackTextToArray(param_RPC_WRITE_NOTE.param1.text, false);
                    
                    string filePath3 = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid2+".html";
                    FileHandle file3 = OpenFile(filePath3, FileMode.WRITE);
                    string text3;
                    if ( file3 )
                    {
                        Print("WrittenNoteData.c RPC_WRITE_NOTE : DZRN : println to filePath3 file " + filePath3);
                        Print("WrittenNoteData.c RPC_WRITE_NOTE : m_ArrayFromFile_RPC_WRITE_NOTE.Count() " + m_ArrayFromFile_RPC_WRITE_NOTE.Count());
                        //FPrintln(file, "<body>");
                        for (int i3 = 0; i3 < m_ArrayFromFile_RPC_WRITE_NOTE.Count(); i3++; )
                        {
                            text3 +=  m_ArrayFromFile_RPC_WRITE_NOTE.Get(i3);
                            //FPrintln(file3, m_ArrayFromFile_RPC_WRITE_NOTE.Get(i3));
                            FPrint(file3, m_ArrayFromFile_RPC_WRITE_NOTE.Get(i3));
                            
                        }
                        //FPrintln(file, "</body>");
                        //Print("FPrintln(file, text):" + text);
                        CloseFile(file3);
                    }
                    else
                    {
                        Print("WrittenNoteData.c RPC_WRITE_NOTE : filePath3 not found " + filePath3);
                    };		
                    //saveing to html locally
                    
                    strFileParam = new array<string>;
                    strFileParam2 = new array<string>;
                    
                    /*
                        strFileParam2 = new array<string>;
                        line_content = text3; 
                        line_content.Split( "<body>", strFileParam );
                        
                        Print("==========SPLIT======0=====");
                        Print(strFileParam.Get(0));
                        Print("================1=====");
                        Print(strFileParam.Get(1));
                        
                        strFileParam.Get(1).Split( "</body>", strFileParam2 );
                        
                        Print("==========SPLIT2=======0====");
                        Print(strFileParam2.Get(0));
                        Print("=================1====");
                        Print(strFileParam2.Get(1));
                    */
                    
                    //deHTML
                    
                    ref array<string, string> decoded_html = DZR_Notes().DecodeHtml(text3)
                    Print(decoded_html);
                    
                    
                    line_content = decoded_html.Get(0);
                    //m_SimpleText = line_content;
                    //SetNoteText(line_content);
                    
                    g_Game.GetMission().SetNoteMenu(g_Game.GetUIManager().EnterScriptedMenu(MENU_NOTE, GetGame().GetUIManager().GetMenu()));
                    if (GetNoteInfo(pen,paper))
                    {
                        
                        if(pen.IsKindOf("DZR_AdminPen"))
                        {
                            g_Game.GetMission().GetNoteMenu().InitNoteWrite(paper,pen,"DZR_ADMIN_NOTE#"+line_content);
                        }
                        else
                        {
                            g_Game.GetMission().GetNoteMenu().InitNoteWrite(paper,pen,notePid2);
                        }
                        
                        
                        
                    }
                    
                }
                
            }
        }
		
		
		//sent from client (NoteMenu), executed on server
		if (rpc_type == ERPCs.RPC_WRITE_NOTE_CLIENT)
		{
            /**
                TODO: Check if already written, and don't replace item. Just rewrite txt file.
            **/
            
            Print("\r\n==========RPC_WRITE_NOTE_CLIENT");
            
            
            if (ctx.Read(param))
            {
                string m_TextFromFile = DZR_Notes().UnpackText(param.param2.text);
                string CreatedBy = param.param2.CreatedBy;
                string CreatedBySteamId = param.param2.CreatedBySteamId;
                //NoteTitle = data.param2.m_NoteTitle;
                string NoteTitle = param.param5;
                Print(param.param2.NoteTitle);
                Print(param.param2.signers);
                Print(param.param2.signers_dates);
                Print(param.param5);
                
                
                
                if (GetNoteInfo(pen, paper))
                {
                    
                    TransferTextToPaperWritten lambda;
                    
                    Pen_ColorBase thePen = Pen_ColorBase.Cast(pen);
                    /*
                        class TransferTextToPaperWritten : ReplaceItemWithNewLambdaBase
                        {
                        protected ref WrittenNoteData m_NoteContents;
                        
                        void TransferTextToPaperWritten(EntityAI old_item, string new_item_type, ref WrittenNoteData data)
                        {
                        m_NoteContents = data;
                        }
                        
                        override void CopyOldPropertiesToNew(notnull EntityAI old_item, EntityAI new_item)
                        {
                        super.CopyOldPropertiesToNew(old_item, new_item);
                        DZR_PaperWritten note = DZR_PaperWritten.Cast(new_item);
                        note.SetPaperWrittenText(m_NoteContents);
                        //m_NoteContents = "";
                        }
                        }
                    */
                    if(thePen)
                    {
                        m_SimpleText = "DEPRECATED WrittenNoteData";
                        // lambda = new TransferTextToPaperWritten(paper, thePen.Result(), this, param.param2.text, CreatedBy, CreatedBySteamId, NoteTitle);
                        Print("TRANSFER DISABLED. Lambda:");
                        Print(paper);
                        Print(thePen.Result());
                        Print(param.param2.text);
                        Print(CreatedBy);
                        Print(CreatedBySteamId);
                        Print(NoteTitle);
                        //Print(thePen.Result()+" ("+thePen.GetType()+")");
                    }
                    
                    
                    vector new_note_ori = paper.GetOrientation();
                    vector new_note_pos = paper.GetPosition();
                    
                    float new_note_hp = paper.GetHealth();
                    
                    PlayerBase player = dzrGetPlayerByIdentity(sender);
                    
                    
                    
                    EntityAI new_note;
                    
                    if (player.GetItemInHands() == paper)
                    {
                        //paper.Delete();
                        Print("\r\n\r\n========== IS PAPER IN HANDS ===========");
                        player.SetSynchDirty();
                        
                        //new_note = EntityAI.Cast(GetGame().CreateObject(thePen.Result(), Vector(0, 5000, 0), false, false, false ));
                        
                        
                        
                        EntityAI entity_in_hands = player.GetHumanInventory().GetEntityInHands();
                        
                        if (entity_in_hands && player.CanDropEntity(entity_in_hands))
                        {
                            Print("\r\n\r\n========== CAN DROP ===========");
                            Print(entity_in_hands);
                            
                            if(player.PredictiveDropEntity(ItemBase.Cast(entity_in_hands)))
                            {
                                Print("\r\n\r\n========== DROPPED ===========");
                            }
                            else
                            {
                                
                                Print("\r\n\r\n========== NOT DROPPED ===========");
                            }
                        }
                        else
                        {
                            Print("\r\n\r\n========== FALSE: entity_in_hands && player.CanDropEntity(entity_in_hands) ===========");
                            Print(entity_in_hands);
                            Print(player.CanDropEntity(entity_in_hands));
                            
                        }
                        /* if (GetGame().IsMultiplayer() && GetGame().IsServer()) */
                        
                        int b1, b2, b3, b4;
                        string notePid3;
                        //ItemBase paper_item = ItemBase.Cast( player.GetItemInHands() );
                        //ItemBase paper_item = ItemBase.Cast( m_Paper );
                        EntityAI target_object = paper;
                        if(target_object)
                        {
                            target_object.GetPersistentID(b1, b2, b3, b4);
                        }
                        
                        if(target_object && !target_object.IsKindOf("DZR_PaperWritten"))
                        {
                            notePid3 = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
                            string source = "$profile:DZR/dzr_notes/notes_db/personal/"+notePid3+".html";
                            string dest = "$profile:DZR/dzr_notes/notes_db/archive/"+notePid3+".html";
                            CopyFile(source,dest);
                            DeleteFile(source);
                            
                            
                            GetGame().ObjectDeleteOnClient(paper);
                            GetGame().ObjectDelete(paper);
                            paper = null;
                            player.SetSynchDirty();
                            entity_in_hands = player.GetHumanInventory().GetEntityInHands();
                            Print("\r\n\r\n========== CHECKING DROPPED ===========");
                            if(!entity_in_hands)
                            {
                                
                                Print("\r\n\r\n========== REALLY DROPPED. Deleting. ===========");
                                if (GetGame().IsMultiplayer() && GetGame().IsServer())
                                {
                                    //paper.Delete();
                                    new_note = EntityAI.Cast(player.GetHumanInventory().CreateInHands(thePen.Result()));
                                }
                            }
                            else
                            {
                                Print("\r\n\r\n========== STILL IN HANDS! WTF!? ===========");
                            }
                            
                            Print(new_note);
                            /*
                                EntityAI ntarget = EntityAI.Cast(new_note);
                                if (player)
                                {
                                InventoryMode invMode = InventoryMode.PREDICTIVE;
                                player.TakeEntityToHandsImpl(invMode, ntarget);
                                }
                            */
                            //player.ServerTakeEntityToHands(new_note);
                            
                            
                            //CreateInHands  player.ServerReplaceItemInHandsWithNew(lambda);
                        }
                        
                    }
                    else
                    {
                        if(target_object)
                        {
                            if(!target_object.IsKindOf("DZR_PaperWritten"))
                            {
                                paper.Delete();
                                Print("\r\n\r\n========== NOT IN HANDS ===========");
                                //    player.ServerReplaceItemWithNew(lambda);
                                new_note = EntityAI.Cast(GetGame().CreateObjectEx(thePen.Result(), new_note_pos, ECE_OBJECT_SWAP, RF_ORIGINAL));
                                new_note.SetOrientation(new_note_ori);
                            }
                        }
                    }
                    
                    if(target_object)
                    {
                        if(target_object.IsKindOf("DZR_PaperWritten"))
                        {
                            new_note = paper;
                        }
                    }
                    Print(new_note);
                    if(new_note)
                    {
                    new_note.SetHealth(new_note_hp);
                    WriteNewNote(new_note, param.param2 , CreatedBySteamId, NoteTitle, CreatedBy );
                    };
                    //WriteNewNote(new_note, param);
                    
                }
                
            }
            
        }
        if (rpc_type == ERPCs.RPC_READ_NOTE)
        {
            
            ref array<string> m_ArrayFromFile_RPC_READ_NOTE = new array<string>;
            Param2<ref JsonSaveData, string> param_RPC_READ_NOTE;
            //Param2<ref JsonSaveData, string> param3;// = new Param1<ref JsonSaveData>();
            
            if (ctx.Read(param_RPC_READ_NOTE))
            {
                /*
                    Print("param_RPC_READ_NOTE: " + param_RPC_READ_NOTE);
                    Print("param_RPC_READ_NOTE.param1: " + param_RPC_READ_NOTE.param1);
                    Print("param_RPC_READ_NOTE.param1.text: " + param_RPC_READ_NOTE.param1.text);
                    Print("param_RPC_READ_NOTE.param1.text[0]: " + param_RPC_READ_NOTE.param1.text[0]);
                    Print("param_RPC_READ_NOTE.param1.text[1]: " + param_RPC_READ_NOTE.param1.text[1]);
                    string m_ObjectFileName = "$profile:DZR/dzr_notes/temp/json_from_server2.json";
                    JsonFileLoader<JsonSaveData>.JsonSaveFile(m_ObjectFileName, param_RPC_READ_NOTE.param1);
                    m_JsonTextFromFile = param_RPC_READ_NOTE.param1;
                    m_TextFromFile = DZR_Notes().UnpackText(param_RPC_READ_NOTE.param1.text);
                    SetNoteText(m_TextFromFile);
                    
                    Print("m_TextFromFile: " + m_TextFromFile);
                    //SetNoteText(param_RPC_READ_NOTE.param1);
                */
                
                
                //getting PID
                
                notePid2 = param_RPC_READ_NOTE.param2;
                //getting PID
                
                
                //saveing to html locally
                
                m_ArrayFromFile_RPC_READ_NOTE = DZR_Notes().UnpackTextToArray(param_RPC_READ_NOTE.param1.text, false);
                
                
                
                string filePath2 = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid2+".html";
                FileHandle file2 = OpenFile(filePath2, FileMode.WRITE);
                string text2;
                if ( file2 )
                {
                    Print("WrittenNoteData.c ERPCs.RPC_READ_NOTE : DZRN : println to file filePath2 " + filePath2);
                    Print("WrittenNoteData.c ERPCs.RPC_READ_NOTE : m_ArrayFromFile_RPC_READ_NOTE.Count() " + m_ArrayFromFile_RPC_READ_NOTE.Count());
                    //FPrintln(file, "<body>");
                    for (int i2 = 0; i2 < m_ArrayFromFile_RPC_READ_NOTE.Count(); i2++; )
                    {
                        text2 +=  m_ArrayFromFile_RPC_READ_NOTE.Get(i2);
                        Print(m_ArrayFromFile_RPC_READ_NOTE.Get(i2));
                        FPrint(file2, m_ArrayFromFile_RPC_READ_NOTE.Get(i2));
                    }
                    Print(text2);
                    
                    //FPrintln(file, "</body>");
                    //Print("FPrintln(file, text):" + text);
                    CloseFile(file2);
                }
                else
                {
                    Print("WrittenNoteData.c RPC_READ_NOTE filePath2 : not found " + filePath2);
                };		
                //saveing to html locally
                
            }
            
            if (ctx.Read(param))
            {
                //SetNoteText(param.param1);
                //SetNoteText(text2);
            }
            
            g_Game.GetMission().SetNoteMenu( g_Game.GetUIManager().EnterScriptedMenu(MENU_NOTE, GetGame().GetUIManager().GetMenu()) ); //NULL means no parent
            
            g_Game.GetMission().GetNoteMenu().InitNoteRead( notePid2 );
            
        }
        
        
        
    }
    
    protected void WriteNewNote(EntityAI new_item_ai, ref JsonSaveData note_data , string m_CreatedBySteamID, string m_NoteTitle ,string m_CreatedBy)
    {
        
        //EntityAI m_NewNote;
        //string m_CreatedBy = note_data.m_CreatedBy;
        //string m_NoteTitle = note_data.param5;
        //string m_CreatedBySteamID = note_data.param2.m_CreatedBySteamID;
        DZR_PaperWritten new_item = DZR_PaperWritten.Cast(new_item_ai);
        new_item.m_DZR_NoteTitle = m_NoteTitle;
        
        ref array<string> packedContent = note_data.text;
        ref array<string> signers_arr = note_data.signers;
        ref array<string> signers_dates_arr = note_data.signers_dates;
        string signers;
        string signers_dates;
        int sign_i = 0;
        foreach (string signer: signers_arr)
        {
            signers += signer+",";
            signers_dates += signers_dates_arr.Get(sign_i)+",";
            sign_i++;
        }
        
        hndDebugPrint("[inv] ReplaceItemWithNewLambdaBase Step H) OnSuccess=" + new_item);
        Print("\r\n\r\n -------------- NEW NOTE");
        Print(new_item);
        ref array<string> m_ArrayFromFile_OnSuccess = new array<string>;		
        //getting PID
        string notePid4;
        int b1, b2, b3, b4;
        EntityAI target_object = new_item;
        //Print("EntityAI.Cast(m_Paper): "+target_object);
        //Print("m_Paper: "+m_Paper);
        if(target_object)
        {
            target_object.GetPersistentID(b1, b2, b3, b4);
        }
        notePid4 = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
        //Print("WrittenNoteData.c sending lambda.m_NewNote pid: "+notePid4);
        if(notePid4 == "0000")
        {
            //Print("WrittenNoteData.c: NO PID");
        };
        //getting PID
        //saveing to html locally
        
        m_ArrayFromFile_OnSuccess = DZR_Notes().UnpackTextToArray(packedContent, true);
        Print("OnSuccess:::: UnpackTextToArray");
        Print(m_ArrayFromFile_OnSuccess);
        //string unpackedText = DZR_Notes().UnpackText(packedContent);
        string filePath;
        if(new_item.IsKindOf("DZR_PaperWrittenAdmin_Static"))
        {
            filePath = "$profile:DZR\\dzr_notes\\notes_db\\static_admin\\"+notePid4+".html";
        }
        else
        {
            filePath = "$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid4+".html";
        }
        FileHandle file = OpenFile(filePath, FileMode.WRITE);
        string text;
        if ( file )
        {
            //Print("WrittenNoteData.c RPC_WRITE_NOTE_CLIENT : println to file " + filePath);
            //Print("WrittenNoteData.c RPC_WRITE_NOTE_CLIENT m_ArrayFromFile.Count() " + m_ArrayFromFile.Count());
            FPrintln(file, "<!DOCTYPE html>");
            FPrintln(file, "	<html>");
            FPrintln(file, "	<head>");
            FPrintln(file, "		<meta http-equiv='content-type' content='text/html; charset=UTF-8'/>");
            FPrintln(file, "		<dzr_meta>");
            FPrintln(file, "		<author values='"+m_CreatedBy+";"+m_CreatedBySteamID+"' />");
            FPrintln(file, "		<signers values='"+signers+"' />"); 
            FPrintln(file, "		<signers_dates values='"+signers_dates+"' />"); 
            FPrintln(file, "		</dzr_meta>");
            FPrintln(file, "	</head>");
            FPrintln(file, "	<body>");
            
            /*
                
                Центральный банк в эту пятницу довольно неожиданно для многих оставил ключевую ставку на уровне 21%. Спросили у экспертов, как такое решение скажется на рубле.
                
                — Для рубля ставка ЦБ сейчас имеет слабое влияние, как было до 2022 года, — арбитраж невозможен. Но есть иные факторы поддержки курса — положительный торговый баланс, восстановление механизмов расчетов за экспорт, ненулевая вероятность геополитического послабления и снижения риск-премии. По нашим оценкам, доллар у 100, а юань под 14 — это адекватные уровни на конец 2024-го — начало 2025 года, — пояснили корреспонденту 72.RU в «БКС Мир инвестиций».
                
                Реклама
                
            */
            
            
            
            //FPrintln(file, "		<attributes values=\""+unp_IsCopy+";"+unp_canCopy+";"+unp_TitleColor+";"+unp_TextColor+";"+unp_Completed+"\"  />");
            text.Replace("\r", "");
            text.Replace("\n", "");
            FPrintln(file, "        <p>");
            for (int i = 0; i < m_ArrayFromFile_OnSuccess.Count(); i++; )
            {
                text +=  m_ArrayFromFile_OnSuccess.Get(i);
                Print("\r\n\r\nADDING LINE:");
                Print(m_ArrayFromFile_OnSuccess.Get(i));
                FPrint(file, m_ArrayFromFile_OnSuccess.Get(i));
            };
            FPrintln(file, "</p>");
            FPrintln(file, "	</body>");
            FPrintln(file, "</html>");
            
            //Print("FPrintln(file, text):" + text);
            CloseFile(file);
        }
        else
        {
            Print("WrittenNoteData.c RPC_WRITE_NOTE_CLIENT : not found " + filePath);
        };	
        
    }
    
    PlayerBase dzrGetPlayerByIdentity(PlayerIdentity identity)
    {
        if(identity)
        {
            int high, low;
            if (!GetGame().IsMultiplayer())
            {
                return PlayerBase.Cast(GetGame().GetPlayer());
            }
            
            GetGame().GetPlayerNetworkIDByIdentityID(identity.GetPlayerId(), low, high);
            return PlayerBase.Cast(GetGame().GetObjectByNetworkId(low, high));
        }
        return NULL;
    }
    
    override void InitNoteInfo(ItemBase pen = null, ItemBase paper = null)
    {
        m_WritingImplement = pen;
        m_Paper = paper;
        //m_Handwriting = handwriting;
    }
    
    override bool GetNoteInfo(out ItemBase pen, out ItemBase paper)
    {
        pen = m_WritingImplement;
        paper = m_Paper;
        //handwriting = m_Handwriting;
        return pen && paper;
    }
    
    override string GetNoteText()
    {		
        return m_SimpleText;
        //return m_TextFromFile;
    }
    
    string GetNotePid()
    {
        return m_Pid;
        //return m_TextFromFile;
    }
    
    PlayerBase GetPlayer()
    {
        return m_Player;
        //return m_TextFromFile;
    }
    
    override void SetNoteText(string text)
    {
        m_SimpleText = "DZR Notes: DEPRECATED";//MiscGameplayFunctions.SanitizeString(text);
        //m_TextFromFile = MiscGameplayFunctions.SanitizeString(text);
    }
    
    override void DepleteWritingImplement(notnull ItemBase pen,string old_text,string new_text)
    {
        float qty_per_char = 1.0;
        float decrease = Math.Clamp((new_text.Length() - old_text.Length()),0,pen.GetQuantityMax());
        pen.AddQuantity(-(qty_per_char * decrease));
    }
    
    JsonSaveData SendFileToClient(string m_TxtFileName)
    {
        ref array<string> compressed = new array<string>;
        JsonSaveData packed_text_object = new JsonSaveData();
        
        // READ FILE CONTENTS
        string content;
        FileHandle fhandle;
        if (FileExist(m_TxtFileName))
        {
            Print("WrittenNoteData.c JsonSaveData SendFileToClient m_TxtFileName");
            Print(m_TxtFileName);
            fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
            string line_content;
            while ( FGets( fhandle,  line_content ) > 0 )
            {
                content += line_content;
            }
            CloseFile(fhandle);
        }
        else
        {
            Print("WrittenNoteData.c NO FILE! "+ m_TxtFileName);
        }
        // READ FILE CONTENTS
        compressed = DZR_Notes().PackText(content);
        packed_text_object.text = compressed;
        
        
        
        //save it
        //string m_SaveObjectFileName = "$profile:DZR/dzr_notes/pre_saved_text.json";
        //JsonFileLoader< JsonSaveData >.JsonSaveFile(m_SaveObjectFileName, importedConfig.text);
        //JsonFileLoader< array<string> >.JsonSaveFile(m_SaveObjectFileName, compressed);
        //save it
        
        return packed_text_object;
        //ref Param1<JsonSaveData> m_Data = new Param1<JsonSaveData>(packed_text_object);
        //GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", m_Data, true, identity);	
        //Print("[dzr_notes] ::: Sent RPC to client");
    }
}
