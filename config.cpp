//TODO: Server Crash on store load. Written data is corrupt. Do not save written data to db.
/**
    Нужно обнулять текст перед сохранением.
    Сейчас не пишется текст, из-за разницы m_NoteContents
	
    override void OnStoreSave(ParamsWriteContext ctx)
	{
    super.OnStoreSave(ctx);
    
    ctx.Write(m_NoteContents);		
	}
**/
class CfgPatches
{
	class czzzz_DzrNotesScripts
	{
		requiredAddons[] = {"DZ_Data","DZ_Gear_Consumables","czzzz_DzrNotes"};
		units[] = {};
		weapons[] = {};
    };
};
class CfgMods
{
	class czzzz_DzrNotesScripts
	{
		type = "mod";
		author = "DayZRussia";
		description = "Write immersive notes.";
		dir = "czzzz_DzrNotesScripts";
		name = "DZR Notes";
		dependencies[] = {"Core","Game","World","Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"czzzz_DzrNotesScripts/Common"};
            };
			class gameLibScriptModule
			{
				files[] = {"czzzz_DzrNotesScripts/Common"};
            };
			class gameScriptModule
			{
				files[] = {"czzzz_DzrNotesScripts/Common","czzzz_DzrNotesScripts/3_Game"};
            };
			class worldScriptModule
			{
				files[] = {"czzzz_DzrNotesScripts/Common","AdditionalMedicSupplies/AdditionalMedicSupplies/scripts/common","czzzz_DzrNotesScripts/4_World"};
            };
			class missionScriptModule
			{
				files[] = {"czzzz_DzrNotesScripts/Common","AdditionalMedicSupplies/AdditionalMedicSupplies/scripts/common","czzzz_DzrNotesScripts/5_Mission"};
            };
        };
    };
};
class CfgSlots
{
	class Slot_DzrPen1
	{
		name = "DzrPen1";
		displayName = "$STR_CfgVehicles_Pen_ColorBase0";
		ghostIcon = "default";
    };
	class Slot_DzrPen2
	{
		name = "DzrPen2";
		displayName = "$STR_CfgVehicles_Pen_ColorBase0";
		ghostIcon = "default";
    };
	class Slot_DzrPen3
	{
		name = "DzrPen3";
		displayName = "$STR_CfgVehicles_Pen_ColorBase0";
		ghostIcon = "default";
    };
	class Slot_DzrPen4
	{
		name = "DzrPen4";
		displayName = "$STR_CfgVehicles_Pen_ColorBase0";
		ghostIcon = "default";
    };
	class Slot_DzrPaper1
	{
		name = "DzrPaper1";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper2
	{
		name = "DzrPaper2";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper3
	{
		name = "DzrPaper3";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper4
	{
		name = "DzrPaper4";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper5
	{
		name = "DzrPaper5";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper6
	{
		name = "DzrPaper6";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper7
	{
		name = "DzrPaper7";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper8
	{
		name = "DzrPaper8";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper9
	{
		name = "DzrPaper9";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper10
	{
		name = "DzrPaper10";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper11
	{
		name = "DzrPaper11";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper12
	{
		name = "DzrPaper12";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper13
	{
		name = "DzrPaper13";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper14
	{
		name = "DzrPaper14";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper15
	{
		name = "DzrPaper15";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper16
	{
		name = "DzrPaper16";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper17
	{
		name = "DzrPaper17";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper18
	{
		name = "DzrPaper18";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper19
	{
		name = "DzrPaper19";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
	class Slot_DzrPaper20
	{
		name = "DzrPaper20";
		displayName = "$STR_CfgVehicles_Paper0";
		ghostIcon = "set:dayz_inventory image:paper";
    };
};
class CfgVehicles
{
	class DuctTape;
	class DZR_3MDuctTape: DuctTape
	{
		scope = 2;
		displayName = "$STR_DZR_3mDucTape";
		descriptionShort = "$STR_DZR_3mDucTape_desc";
		model = "\dz\gear\consumables\DuctTape.p3d";
		OnRestrainChange = "DuctTapeLocked";
		isMeleeWeapon = 1;
		weight = 500;
		absorbency = 0;
		itemSize[] = {2,2};
		rotationFlags = 17;
		stackedUnit = "percentage";
		quantityBar = 1;
		varQuantityInit = 100;
		varQuantityMin = 0;
		varQuantityMax = 100;
		repairKitType = 5;
		hiddenSelectionsTextures[] = {"#(argb,8,8,3)color(0.35,0.36,0.28,1.0,CO)","#(argb,8,8,3)color(0.15,0.15,0.15,1.0,CO)"};
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 50;
					healthLevels[] = {{1,{"DZ\gear\consumables\data\duct_tape.rvmat"}},{0.7,{"DZ\gear\consumables\data\duct_tape.rvmat"}},{0.5,{"DZ\gear\consumables\data\duct_tape_damage.rvmat"}},{0.3,{"DZ\gear\consumables\data\duct_tape_damage.rvmat"}},{0,{"DZ\gear\consumables\data\duct_tape_destruct.rvmat"}}};
                };
            };
        };
		class AnimEvents
		{
			class SoundWeapon
			{
				class rope_untie
				{
					soundSet = "rope_untie_SoundSet";
					id = 202;
                };
				class rope_struggle
				{
					soundSet = "rope_struggle_SoundSet";
					id = 203;
                };
				class ducttape_tieup
				{
					soundSet = "ducttape_tieup_SoundSet";
					id = 13338;
                };
				class rope_tieup_end
				{
					soundSet = "rope_tieup_end_SoundSet";
					id = 13339;
                };
				class rope_tieup_back
				{
					soundSet = "rope_tieup_back_SoundSet";
					id = 13340;
                };
            };
        };
    };
	class Inventory_Base;
	class Pen_ColorBase: Inventory_Base
	{
		inventorySlot[] += {"DzrPen1","DzrPen2","DzrPen3","DzrPen4"};
    };
	class Paper: Inventory_Base
	{
		model = "\dzr_notes\dzr_paper_blank.p3d";
		inventorySlot[] += {"Paper","DzrPaper1","DzrPaper2","DzrPaper3","DzrPaper4","DzrPaper5","DzrPaper6","DzrPaper7","DzrPaper8","DzrPaper9","DzrPaper11","DzrPaper12","DzrPaper13","DzrPaper14","DzrPaper15","DzrPaper16","DzrPaper17","DzrPaper18","DzrPaper19","DzrPaper20"};
		hiddenSelections[] = {"zbytek","camoground"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_paper_blank_co.paa","\dzr_notes\data\dzr_paper_blank_co.paa"};
    };
	class DZR_PaperWrittenAdmin: Paper
	{
		canBeSplit = 0;
		varStackMax = 1;
		scope = 2;
		inventorySlot[] = {"Paper","DzrPaper1","DzrPaper2","DzrPaper3","DzrPaper4","DzrPaper5","DzrPaper6","DzrPaper7","DzrPaper8","DzrPaper9","DzrPaper11","DzrPaper12","DzrPaper13","DzrPaper14","DzrPaper15","DzrPaper16","DzrPaper17","DzrPaper18","DzrPaper19","DzrPaper20","Paper","PaperWritten","DzrPaper1","DzrPaper2","DzrPaper3","DzrPaper4","DzrPaper5","DzrPaper6","DzrPaper7","DzrPaper8","DzrPaper9","DzrPaper11","DzrPaper12","DzrPaper13","DzrPaper14","DzrPaper15","DzrPaper16","DzrPaper17","DzrPaper18","DzrPaper19","DzrPaper20"};
		model = "\dzr_notes\dzr_paper_written.p3d";
		displayName = "$STR_DZR_Paper_WrittenAdmin";
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_paper_co.paa"};
		descriptionShort = "$STR_DZR_Paper_WrittenAdmin_DESC";
    };
	class DZR_PaperWrittenAdmin_Red: DZR_PaperWrittenAdmin
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_red_doc_paper_co.paa"};
    };
	class DZR_PaperWrittenAdmin_Green: DZR_PaperWrittenAdmin
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_green_doc_paper_co.paa"};
    };
	class DZR_PaperWrittenAdmin_Blue: DZR_PaperWrittenAdmin
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_blue_doc_paper_co.paa"};
    };
	class DZR_PaperWrittenAdmin_Static: DZR_PaperWrittenAdmin
	{
		displayName = "$STR_DZR_Paper_Written";
		descriptionShort = "$STR_DZR_Paper_Written_DESC";
    };
	class DZR_PaperWrittenAdmin_Red_Static: DZR_PaperWrittenAdmin
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_red_doc_paper_co.paa"};
    };
	class DZR_PaperWrittenAdmin_Green_Static: DZR_PaperWrittenAdmin
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_green_doc_paper_co.paa"};
    };
	class DZR_PaperWrittenAdmin_Blue_Static: DZR_PaperWrittenAdmin
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\dzr_blue_doc_paper_co.paa"};
    };
	class DZR_AdminPen: Pen_ColorBase
	{
		scope = 2;
		displayName = "$STR_DZR_Admin_Pen";
		descriptionShort = "$STR_DZR_Admin_Pen_DESC";
    };
	class DZR_AdminPen_Red: DZR_AdminPen
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\loot_pen_red_star_co.paa"};
    };
	class DZR_AdminPen_Green: DZR_AdminPen
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\loot_pen_green_star_co.paa"};
    };
	class DZR_AdminPen_Blue: DZR_AdminPen
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\loot_pen_blue_star_co.paa"};
    };
	class DZR_AdminPen_Static: DZR_AdminPen
	{
		scope = 2;
		displayName = "$STR_DZR_Admin_Pen_Static";
		descriptionShort = "$STR_DZR_Admin_Pen_Static_DESC";
    };
	class DZR_AdminPen_Red_Static: DZR_AdminPen_Static
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\loot_pen_red_star_co.paa"};
    };
	class DZR_AdminPen_Green_Static: DZR_AdminPen_Static
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\loot_pen_green_star_co.paa"};
    };
	class DZR_AdminPen_Blue_Static: DZR_AdminPen_Static
	{
		hiddenSelections[] = {"zbytek"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\loot_pen_blue_star_co.paa"};
    };
	class DZR_File_Folder: Inventory_Base
	{
		scope = 2;
		displayName = "$STR_DZR_File_Folder";
		descriptionShort = "$STR_DZR_File_Folder_DESC";
		simulation = "ItemBook";
		model = "\DZ\gear\books\Book_kniga.p3d";
		inventorySlot[] += {"Book"};
		itemSize[] = {2,2};
		absorbency = 0.9;
		attachments[] = {"DzrPen1","DzrPen2","DzrPen3","DzrPen4","DzrPaper1","DzrPaper2","DzrPaper3","DzrPaper4","DzrPaper5","DzrPaper6","DzrPaper7","DzrPaper8","DzrPaper9","DzrPaper11","DzrPaper12","DzrPaper13","DzrPaper14","DzrPaper15","DzrPaper16","DzrPaper17","DzrPaper18","DzrPaper19","DzrPaper20"};
		hiddenSelections[] = {"camoGround"};
		hiddenSelectionsTextures[] = {"\dzr_notes\data\book_folder_co.paa"};
		quantityShow = 0;
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 100;
					healthLevels[] = {{1,{"DZ\gear\books\Data\book.rvmat"}},{0.7,{"DZ\gear\books\Data\book.rvmat"}},{0.5,{"DZ\gear\books\Data\book_damage.rvmat"}},{0.3,{"DZ\gear\books\Data\book_damage.rvmat"}},{0,{"DZ\gear\books\Data\book_destruct.rvmat"}}};
                };
            };
        };
    };
	class DZR_File_Folder_Grey: DZR_File_Folder
	{
		hiddenSelectionsTextures[] = {"\dzr_notes\data\book_folder_grey_co.paa"};
    };
	class DZR_File_Folder_Red: DZR_File_Folder
	{
		hiddenSelectionsTextures[] = {"\dzr_notes\data\book_folder_red_co.paa"};
    };
	class DZR_File_Folder_Green: DZR_File_Folder
	{
		hiddenSelectionsTextures[] = {"\dzr_notes\data\book_folder_green_co.paa"};
    };
	class DZR_File_Folder_Blue: DZR_File_Folder
	{
		hiddenSelectionsTextures[] = {"\dzr_notes\data\book_folder_blue_co.paa"};
    };
	class ItemMap: Inventory_Base
	{
		inventorySlot[] += {"DzrPaper1","DzrPaper2","DzrPaper3","DzrPaper4","DzrPaper5","DzrPaper6","DzrPaper7","DzrPaper8","DzrPaper9","DzrPaper11","DzrPaper12","DzrPaper13","DzrPaper14","DzrPaper15","DzrPaper16","DzrPaper17","DzrPaper18","DzrPaper19","DzrPaper20"};
    };
};
