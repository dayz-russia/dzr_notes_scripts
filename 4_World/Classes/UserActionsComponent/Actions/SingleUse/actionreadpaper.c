modded class ActionReadPaper
{
    
    PlayerBase dzr_m_Player;
	override bool ActionCondition( PlayerBase player, ActionTarget target, ItemBase item )
	{
		Paper target_object = Paper.Cast(target.GetObject());
        dzr_m_Player = player;
		if (player.IsPlacingLocal() && target_object && !target_object.IsReadable())
		return false;
		#ifdef AMS_AdditionalMedicSupplies
			Print("FIXING AMS for: "+item );
			if ( item && item.IsKindOf( "AMS_TestKitReport" ) )
			{
				return false;
            };
        #endif
		//Print("ActionReadPaper.c");
		return true;
    };	

};


modded class ActionReadPaperCB
{
	override void OnStateChange(int pOldState, int pCurrentState)
	{
        if (pCurrentState == STATE_NONE && (!GetGame().IsDedicatedServer()))
        {
            if (GetGame().GetUIManager() && GetGame().GetUIManager().IsMenuOpen(MENU_NOTE))
            GetGame().GetUIManager().FindMenu(MENU_NOTE).Close();
        }
        else if (pCurrentState == STATE_LOOP_LOOP && pOldState != STATE_LOOP_LOOP && (!GetGame().IsMultiplayer() || GetGame().IsServer()))
        {
            Print(GetGame().IsServer());
            {
                Print("OnStateChange GetGame().IsClient()");
                Print(GetGame().IsClient());
                Print("OnStateChange GetGame().IsServer()");
                Print(GetGame().IsServer());
            }
            if(GetGame().IsClient())
            {
                Print("OnStateChange GetGame().IsClient()");
                Print(GetGame().IsClient());
                Print("OnStateChange GetGame().IsServer()");
                Print(GetGame().IsServer());
            }
            //getting PID
            PlayerBase player = PlayerBase.Cast( m_ActionData.m_Player );
            ItemBase paper_item = ItemBase.Cast( player.GetItemInHands() );
            int b1, b2, b3, b4;
            string notePid;
            EntityAI target_object = paper_item ;
            if(target_object)
            {
                target_object.GetPersistentID(b1, b2, b3, b4);
            }
            
            notePid = b1.ToString() + b2.ToString() + b3.ToString() + b4.ToString();
            //del
            
            if(notePid == "0000")
            {
                Print("NO PID");
            };
            //getting PID
            
            JsonSaveData FileData = new JsonSaveData();
            FileData = SendFileToClient("$profile:DZR\\dzr_notes\\notes_db\\personal\\"+notePid+".html");
            
            Param2<ref JsonSaveData, string> text = new Param2<ref JsonSaveData, string>(FileData, notePid);
            //Print("actionreadpaper.c notePid:" + notePid);
            //Print("actionreadpaper.c OnStateChange FileData.text[0]:" + FileData.text[0]);
            //Print("actionreadpaper.c OnStateChange FileData.text:" + FileData.text);
            //Print("actionreadpaper.c OnStateChange FileData:" + FileData);
            //Param1<JsonSaveData> text = new Param1<JsonSaveData>(paper_item.GetWrittenNoteData().GetNoteText());
            paper_item.RPCSingleParam(ERPCs.RPC_READ_NOTE, text, true,m_ActionData.m_Player.GetIdentity());
        }
    }
	
	JsonSaveData SendFileToClient(string m_TxtFileName)
	{
        
        Print(GetGame().IsServer());
        {
            Print("SendFileToClient GetGame().IsClient()");
            Print(GetGame().IsClient());
            Print("SendFileToClient GetGame().IsServer()");
            Print(GetGame().IsServer());
        }
        if(GetGame().IsClient())
        {
            Print("SendFileToClient GetGame().IsClient()");
            Print(GetGame().IsClient());
            Print("SendFileToClient GetGame().IsServer()");
            Print(GetGame().IsServer());
        }
        
        
		ref array<string> compressed = new array<string>;
		JsonSaveData packed_text_object = new JsonSaveData();
        
		// READ FILE CONTENTS
		string content;
		FileHandle fhandle;
		if (FileExist(m_TxtFileName))
		{
			Print("actionreadpaper.c Reading m_TxtFileName");
			Print(m_TxtFileName);
			fhandle	=	OpenFile(m_TxtFileName, FileMode.READ);
			string line_content;
			while ( FGets( fhandle,  line_content ) > 0 )
			{
				content += line_content;
            }
			CloseFile(fhandle);
        }
		else
		{
			Print("actionreadpaper.c NO FILE! "+ m_TxtFileName);
        }
		// READ FILE CONTENTS
		compressed = DZR_Notes().PackText(content);
		packed_text_object.text = compressed;
		
        
		
		//save it
		//string m_SaveObjectFileName = "$profile:DZR/dzr_notes/pre_saved_text.json";
		//JsonFileLoader< JsonSaveData >.JsonSaveFile(m_SaveObjectFileName, importedConfig.text);
		//JsonFileLoader< array<string> >.JsonSaveFile(m_SaveObjectFileName, compressed);
		//save it
		
        return packed_text_object;
		//ref Param1<JsonSaveData> m_Data = new Param1<JsonSaveData>(packed_text_object);
		//GetRPCManager().SendRPC( "DZR_NOTES_RPC", "DZR_MissionGameplay_GetFileFromServer2", m_Data, true, identity);	
		//Print("[dzr_notes] ::: Sent RPC to client");
    }
};
